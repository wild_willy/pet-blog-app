import * as React from 'react';
export interface Props {
}
export interface State {
}
export declare class App extends React.Component<Props, State> {
    render(): JSX.Element;
}
