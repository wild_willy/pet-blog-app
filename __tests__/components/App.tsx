import * as React from "react";
import {Provider} from "react-redux";
import * as TestRenderer from "react-test-renderer";
import configureStore from "../../src/store";
import { Navigation } from "../../src/app";


const store = configureStore();
// Have to mock react-native-gesture-handler
// See: https://github.com/react-navigation/react-navigation/issues/5574
jest.mock("react-native-gesture-handler", () => {
  const View = require("react-native/Libraries/Components/View/View");
  return {
    Swipeable: View,
    DrawerLayout: View,
    State: {},
    ScrollView: View,
    Slider: View,
    Switch: View,
    TextInput: View,
    ToolbarAndroid: View,
    ViewPagerAndroid: View,
    DrawerLayoutAndroid: View,
    WebView: View,
    NativeViewGestureHandler: View,
    TapGestureHandler: View,
    FlingGestureHandler: View,
    ForceTouchGestureHandler: View,
    LongPressGestureHandler: View,
    PanGestureHandler: View,
    PinchGestureHandler: View,
    RotationGestureHandler: View,
      /* Buttons */
    RawButton: View,
    BaseButton: View,
    RectButton: View,
    BorderlessButton: View,
      /* Other */
    FlatList: View,
    gestureHandlerRootHOC: jest.fn(),
    Directions: {},
  };
});
it("renders correctly with defaults the application", () => {
  const app = TestRenderer.create(
    <Provider store={store}>
      <Navigation />
    </Provider>,
  ).toJSON();
  expect(app).toMatchSnapshot();
});

