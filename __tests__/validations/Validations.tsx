import { Input, validateInput, validateMultipleInputs } from "../../src/validation/helpers";

test("it should be a valid email", () => {
  const validEmail: string = "ex@ex.com";
  const input: Input<string> = {
    type: "email",
    value: validEmail,
  };
  expect(validateInput(input)).toEqual(null); // A valid value returns null
});

test("it should be an invalid email", () => {
  const invalidEmail: string = "ex.ex.com";
  const input: Input<string> = {
    type: "email",
    value: invalidEmail,
  };
  expect(validateInput(input)).toBe("Email address must be valid");
});

test("it should not be an empty email", () => {
  const input: Input<string> = {
    type: "email",
    value: "",
  };
  expect(validateInput(input)).toBe("Email required");
});

test("it should not be an empty name", () => {
  const input: Input<string> = {
    type: "name",
    value: "",
  };
  expect(validateInput(input)).toBe("Name required");
});

test("it should be a valid name", () => {
  const input: Input<string> = {
    type: "name",
    value: "Jhon Travolta",
  };
  expect(validateInput(input)).toBe(null);
});

test("it should be valid password", () => {
  const input: Input<string> = {
    type: "password",
    value: "passpass",
  };
  expect(validateInput(input)).toBe(null);
});

test("it should be an invalid password too short", () => {
  const input: Input<string> = {
    type: "password",
    value: "pass23",
  };
  expect(validateInput(input)).toBe("Password must be at least 8 characters long");
});

test("it should not be an empty password", () => {
  const input: Input<string> = {
    type: "password",
    value: "",
  };
  expect(validateInput(input)).toBe("Password required");
});

test("it should be valid password and password confirmation", () => {
  const password: Input<string> = {
    type: "password",
    value: "passpass",
  };
  const passwordConfirmation: Input<string> = {
    type: "confirmPassword",
    value: "passpass",
  };
  const result = validateMultipleInputs([password, passwordConfirmation]);
  expect(result.password).toBeUndefined();
  expect(result.confirmPassword).toBeUndefined();
});

test("it should not be a valid confirmation password", () => {
  const password: Input<string> = {
    type: "password",
    value: "passpass",
  };
  const passwordConfirmation: Input<string> = {
    type: "confirmPassword",
    value: "passpas1",
  };
  const result = validateMultipleInputs([password, passwordConfirmation]);
  console.log(result)
  console.log(result["confirmPassword"])
  expect(result.password).toBeUndefined();
  expect(result.confirmPassword[0]).toBe("Passwords are not equal");
});

test("it should not be a valid confirmation password", () => {
  const password: Input<string> = {
    type: "password",
    value: "passpass",
  };
  const passwordConfirmation: Input<string> = {
    type: "confirmPassword",
    value: "",
  };
  const result = validateMultipleInputs([password, passwordConfirmation]);
  expect(result.password).toBeUndefined();
  expect(result.confirmPassword[0]).toBe("Password confirmation required");
});
