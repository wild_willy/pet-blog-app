import {mockStore} from '../../src/test/utils';
import { UserProfile, Tokens } from '../../src/api/Api';
import { FetchingUsersAction, AdminActionTypes, AdminActions, AdminState, AdminStateTag, FetchedUsersAction, FetchErrorAction } from '../../src/store/admin/types';
import { fetchingUsers, fetchedUsers, fetchError } from '../../src/store/admin/actions';
import { adminReducer } from '../../src/store/admin/reducer';

const tokens: Tokens = {
    jwt: "",
    xsrf: ""
}

describe("Admin store", () => {

    it("Creates the fetching users action and alters the state accordingly", () => {
        const store = mockStore();
        const expectedAction: FetchingUsersAction = {
            type: AdminActionTypes.FETCHING_USERS,
            payload: {
                tokens
            }
        };
        store.dispatch(fetchingUsers(tokens));
        const actions: AdminActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: AdminState = store.getState() as AdminState;
        expect(adminReducer(state, actions[0])).toStrictEqual({
            tag: AdminStateTag.FETCHING_USERS,
            tokens
        });
    });


    it("Creates the fetched users action and alters the state accordingly", () => {
        const store = mockStore();
        const users: UserProfile[] = [];
        const expectedAction: FetchedUsersAction = {
            type: AdminActionTypes.FETCHED_USERS,
            payload: {
                users
            }
        };
        store.dispatch(fetchedUsers(users));
        const actions: AdminActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: AdminState = store.getState() as AdminState;
        expect(adminReducer(state, actions[0])).toStrictEqual({
            tag: AdminStateTag.FETCHED_USERS,
            users
        });
    });

    it("Creates the fetch error action and alters the state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: FetchErrorAction = {
            type: AdminActionTypes.FETCH_ERROR,
            payload: {
                error
            }
        };
        store.dispatch(fetchError(error));
        const actions: AdminActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: AdminState = store.getState() as AdminState;
        expect(adminReducer(state, actions[0])).toStrictEqual({
            tag: AdminStateTag.FETCH_ERROR,
            error
        });
    }); 

})
