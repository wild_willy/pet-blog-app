import {mockStore} from '../../src/test/utils';
import {NewUser} from '../../src/api/Api';
import { CreateUserRequest, CREATE_USER_REQUEST, UserActionTypes, UserState, CREATE_USER_ERROR, CreateUserError, CreateUserSuccessful, CREATE_USER_SUCCESSFUL } from '../../src/store/user/types';
import { createUserRequest, createUserError, createUserSuccessful } from '../../src/store/user/actions';
import { userReducer } from '../../src/store/user/reducers';

describe("Create User Store", () => {
    it("Create the create user request action and alters the state accordingly", () => {
        const store = mockStore();
        const newUser: NewUser = {
            unhashedPass: "",
            name: "",
            email: "",
            username: ""
        };
        const expectedAction: CreateUserRequest = {
            type: CREATE_USER_REQUEST,
            payload: {
                user: newUser
            }
        };
        store.dispatch(createUserRequest(newUser));
        const actions: UserActionTypes[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: UserState = store.getState()  as UserState;
        expect(userReducer(state, actions[0])).toStrictEqual({
            newUser
        });
    });

    it("Create the create user error action and alters the state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: CreateUserError = {
            type: CREATE_USER_ERROR,
            payload: {
                error
            }
        };
        store.dispatch(createUserError(error));
        const actions: UserActionTypes[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: UserState = store.getState()  as UserState;
        expect(userReducer(state, actions[0])).toStrictEqual({
            error
        });
    });


    it("Create the create user successful action and alters the state accordingly", () => {
        const store = mockStore();
        const userId: number = 1;
        const newUser: NewUser = {
            unhashedPass: "",
            name: "",
            email: "",
            username: ""
        };
        const expectedAction: CreateUserSuccessful = {
            type: CREATE_USER_SUCCESSFUL,
            payload: {
                userId,
                user: newUser,
            }
        };
        store.dispatch(createUserSuccessful(userId, newUser));
        const actions: UserActionTypes[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: UserState = store.getState()  as UserState;
        expect(userReducer(state, actions[0])).toStrictEqual({
            userId,
            newUser
        });
    });


})