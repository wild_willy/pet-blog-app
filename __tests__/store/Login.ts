import { mockStore } from "../../src/test/utils";
import { Tokens, AuthUser } from '../../src/api/Api';
import { LoginActions, LoginRequestAction, LoginActionTypes, LoginState, LoginStateTag, LoggedAction, LoginErrorAction } from '../../src/store/login/types';
import { LoginForm } from '../../src/api/Api';
import { loginRequest, logged, loginError } from '../../src/store/login/actions';
import { loginReducer } from '../../src/store/login/reducer';

const testTokens: Tokens = {
    jwt: "",
    xsrf: ""
}
describe("Login Store",  () => {
    it("Creates the login action and alters the state accordingly", () => {
        const store = mockStore();
        const loginForm: LoginForm = {
            loginEmail: "",
            loginPass: ""
        }
        const expectedAction: LoginRequestAction = {
            type: LoginActionTypes.LOGIN_REQUEST,
            payload: {
                loginForm
            }
        };
        store.dispatch(loginRequest(loginForm));
        const actions: LoginActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: LoginState = store.getState() as LoginState;
        expect(loginReducer(state, actions[0])).toStrictEqual({
            tag: LoginStateTag.LOGIN_REQUEST,
            loginForm
        })
    });

    it("Creates the logged action and alters the state accordingly", () => {
        const store = mockStore();
        const authUser: AuthUser = {
            authEmail: "",
            authName: "",
            authRole: "",
            authUserId: 0
        }
        const expectedAction: LoggedAction = {
            type: LoginActionTypes.LOGGED,
            payload: {
                profile: authUser,
                tokens: testTokens
            }
        }
        store.dispatch(logged(testTokens, authUser));
        const actions: LoginActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: LoginState = store.getState() as LoginState;
        expect(loginReducer(state, actions[0])).toStrictEqual({
            tag: LoginStateTag.LOGGED,
            profile: authUser,
            tokens: testTokens
        });
    })

    it("Creates the log error action and alters the state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: LoginErrorAction = {
            type: LoginActionTypes.LOGIN_ERROR,
            payload: {
                error
            }
        }
        store.dispatch(loginError(error));
        const actions: LoginActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: LoginState = store.getState() as LoginState;
        expect(loginReducer(state, actions[0])).toStrictEqual({
            tag: LoginStateTag.LOGIN_ERROR,
            error
        });
    })
})