import { mockStore } from "../../../src/test/utils";
import { Tokens, Micropost } from '../../../src/api/Api';
import { PersonalFeedActions, PersonalFeedActionTypes, PersonalFeedState, PersonalFeedStateTag } from '../../../src/store/feed/personal/types';
import { fetchingPosts, fetchedPosts, fetchError } from '../../../src/store/feed/personal/actions';
import { personalFeedReducer } from '../../../src/store/feed/personal/reducer';

const testTokens: Tokens = {
    jwt: "",
    xsrf: ""
}
describe("Personal Feed Store", () => {

    it("Creates the fetching posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const expectedAction: PersonalFeedActions = {
            type: PersonalFeedActionTypes.FETCHING_POSTS,
            payload: {
                tokens: testTokens
            }
        };
        store.dispatch(fetchingPosts(testTokens));
        const actions: PersonalFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: PersonalFeedState = store.getState() as PersonalFeedState;
        expect(personalFeedReducer(state, actions[0])).toStrictEqual({
            tag: PersonalFeedStateTag.FETCHING_POSTS,
            tokens: testTokens
        });
    });

     it("Creates the fetched posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const posts: Micropost[] = [];
        const expectedAction: PersonalFeedActions = {
            type: PersonalFeedActionTypes.FETCHED_POSTS,
            payload: {
                posts
            }
        };
        store.dispatch(fetchedPosts(posts));
        const actions: PersonalFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: PersonalFeedState = store.getState() as PersonalFeedState;
        expect(personalFeedReducer(state, actions[0])).toStrictEqual({
            tag: PersonalFeedStateTag.FETCHED_POSTS,
            posts
        });
    });

 
     it("Creates the fetch error action and alters the store's state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: PersonalFeedActions = {
            type: PersonalFeedActionTypes.FETCH_ERROR,
            payload: {
                error
            }
        };
        store.dispatch(fetchError(error));
        const actions: PersonalFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: PersonalFeedState = store.getState() as PersonalFeedState;
        expect(personalFeedReducer(state, actions[0])).toStrictEqual({
            tag: PersonalFeedStateTag.FETCH_ERROR,
            error
            
        });
    });

})