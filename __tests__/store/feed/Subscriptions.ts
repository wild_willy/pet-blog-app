import { mockStore } from "../../../src/test/utils";
import {  Micropost, Tokens, Subscription } from '../../../src/api/Api';
import { SubsFeedActions, SubsFeedState, SubsFeedStateTags, SubsFeedActionTypes } from '../../../src/store/feed/subscriptions/types';
import { subsFeedReducer } from '../../../src/store/feed/subscriptions/reducer';
import { fetchingPosts, fetchedPosts, fetchErrorPosts, fetchingSubs, fetchedSubs, fetchErrorSubs } from '../../../src/store/feed/subscriptions/actions';


const testTokens: Tokens = {
    jwt: "",
    xsrf: ""
}

describe("Subs Feed Store", () => {

    it("Creates the fetching subs action and alters the store's state accordingly", () => {
        const store = mockStore();
        const expectedAction: SubsFeedActions = {
            type: SubsFeedActionTypes.FETCHING_SUBS,
            payload: {
                tokens: testTokens
            }
        };
        store.dispatch(fetchingSubs(testTokens));
        const actions: SubsFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: SubsFeedState = store.getState() as SubsFeedState;
        expect(subsFeedReducer(state, actions[0])).toStrictEqual({
            tag: SubsFeedStateTags.FETCHING_SUBS,
            tokens: testTokens
        });
    });

    it("Creates the fetched subs action and alters the store's state accordingly", () => {
        const store = mockStore();
        const subs: Subscription[] = [];
        const expectedAction: SubsFeedActions = {
            type: SubsFeedActionTypes.FETCHED_SUBS,
            payload: {
                subs
            }
        };
        store.dispatch(fetchedSubs(subs));
        const actions: SubsFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: SubsFeedState = store.getState() as SubsFeedState;
        expect(subsFeedReducer(state, actions[0])).toStrictEqual({
            tag: SubsFeedStateTags.FETCHED_SUBS,
            subs
        });
    });

    it("Creates the fetch error subs action and alters the store's state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: SubsFeedActions = {
            type: SubsFeedActionTypes.FETCH_ERROR_SUBS,
            payload: {
                error
            }
        };
        store.dispatch(fetchErrorSubs(error));
        const actions: SubsFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: SubsFeedState = store.getState() as SubsFeedState;
        expect(subsFeedReducer(state, actions[0])).toStrictEqual({
            tag: SubsFeedStateTags.FETCH_ERROR_SUBS,
            error
        });
    });

    it("Creates the fetching posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const expectedAction: SubsFeedActions = {
            type: SubsFeedActionTypes.FETCHING_POSTS,
            payload: {
            }
        };
        store.dispatch(fetchingPosts());
        const actions: SubsFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: SubsFeedState = store.getState() as SubsFeedState;
        expect(subsFeedReducer(state, actions[0])).toStrictEqual({
            tag: SubsFeedStateTags.FETCHING_POSTS,
        });
    });

     it("Creates the fetched posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const posts: Micropost[] = [];
        const expectedAction: SubsFeedActions = {
            type: SubsFeedActionTypes.FETCHED_POSTS,
            payload: {
                posts
            }
        };
        store.dispatch(fetchedPosts(posts));
        const actions: SubsFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: SubsFeedState = store.getState() as SubsFeedState;
        expect(subsFeedReducer(state, actions[0])).toStrictEqual({
            tag: SubsFeedStateTags.FETCHED_POSTS,
            posts
        });
    });

 
     it("Creates the fetch error posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: SubsFeedActions = {
            type: SubsFeedActionTypes.FETCH_ERROR_POSTS,
            payload: {
                error
            }
        };
        store.dispatch(fetchErrorPosts(error));
        const actions: SubsFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: SubsFeedState = store.getState() as SubsFeedState;
        expect(subsFeedReducer(state, actions[0])).toStrictEqual({
            tag: SubsFeedStateTags.FETCH_ERROR_POSTS,
            error
            
        });
    });

})