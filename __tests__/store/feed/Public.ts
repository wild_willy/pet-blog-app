import { mockStore } from "../../../src/test/utils";
import { Tokens, Micropost } from '../../../src/api/Api';
import { publicFeedReducer } from '../../../src/store/feed/public/reducer';
import { PublicFeedActions, PublicFeedState, PublicActionTypes, PublicFeedTags } from '../../../src/store/feed/public/types';
import { fetchingPosts, fetchedPosts, fetchError } from '../../../src/store/feed/public/actions';

const testTokens: Tokens = {
    jwt: "",
    xsrf: ""
}
describe("Public Feed Store", () => {

    it("Creates the fetching posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const expectedAction: PublicFeedActions = {
            type: PublicActionTypes.FETCHING_POSTS,
            payload: {
                tokens: testTokens
            }
        };
        store.dispatch(fetchingPosts(testTokens));
        const actions: PublicFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: PublicFeedState = store.getState() as PublicFeedState;
        expect(publicFeedReducer(state, actions[0])).toStrictEqual({
            tag: PublicFeedTags.FETCHING_POSTS,
            tokens: testTokens
        });
    });

     it("Creates the fetched posts action and alters the store's state accordingly", () => {
        const store = mockStore();
        const posts: Micropost[] = [];
        const expectedAction: PublicFeedActions = {
            type: PublicActionTypes.FETCHED_POSTS,
            payload: {
                posts
            }
        };
        store.dispatch(fetchedPosts(posts));
        const actions: PublicFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: PublicFeedState = store.getState() as PublicFeedState;
        expect(publicFeedReducer(state, actions[0])).toStrictEqual({
            tag: PublicFeedTags.FETCHED_POSTS,
            posts
        });
    });

 
     it("Creates the fetch error action and alters the store's state accordingly", () => {
        const store = mockStore();
        const error: string = "error";
        const expectedAction: PublicFeedActions = {
            type: PublicActionTypes.FETCH_ERROR,
            payload: {
                error
            }
        };
        store.dispatch(fetchError(error));
        const actions: PublicFeedActions[] = store.getActions();
        expect(actions[0]).toStrictEqual(expectedAction);
        const state: PublicFeedState = store.getState() as PublicFeedState;
        expect(publicFeedReducer(state, actions[0])).toStrictEqual({
            tag: PublicFeedTags.FETCH_ERROR,
            error
            
        });
    });

})