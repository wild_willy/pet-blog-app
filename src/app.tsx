import * as React from "react";
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";
import {Provider} from "react-redux";
import CreateUser from "./containers/CreateUserContainer";
import Login from "./containers/LoginContainer";
import configureStore from "./store";
import PersonalFeedScreen from './containers/PersonalFeedContainer';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import CreatePostScreen from './containers/CreatePostContainer';
import EditPostScreen from './containers/EditPostContainer';
import { Icon } from 'react-native-elements';
import SubsFeedScreen from './containers/SubsFeedContainer';
import PublicFeedScreen from './containers/PublicFeedContainer';
import AdminScreen from './containers/AdminContainer';

const AuthStack = createStackNavigator({
  LoginScreen: Login,
  CreateUserScreen: CreateUser
})

const PersonalFeedStack = createStackNavigator({
  Personal: PersonalFeedScreen,
  CreatePost: CreatePostScreen,
  EditPost: EditPostScreen,
  AdminUsers: AdminScreen,
});

const FeedTab = createMaterialBottomTabNavigator({
  Personal: {
    screen: PersonalFeedStack,
    navigationOptions: {
      tabBarLabel: "Personal",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="account-circle" size={25} color="#fff" />
      ),
    }
  },
  Subscriptions: {
    screen: SubsFeedScreen,
    navigationOptions: {
      tabBarLabel: "Subs",
      tabBarIcon: ({tintColor}) => (
        <Icon name="favorite" size={25} color="#fff" />
      )
    }
  },
  Public: {
    screen: PublicFeedScreen,
    navigationOptions: {
      tabBarLabel: "Public",
      tabBarIcon: ({tintColor}) => (
        <Icon name="public" size={25} color="#fff" /> 
      )
    }
  }

})

const RootSwitch = createSwitchNavigator({
  AuthStack,
  FeedTab
})


export interface Props {}
export interface State {}

export const Navigation = createAppContainer(RootSwitch);

const store = configureStore();

export class App extends React.Component<Props, State> {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}

