// Type of the validation Dictionary
interface ValidationDictionary {[index: string]: any; }

// Validation dictionary
export const validationDictionary: ValidationDictionary = {
  email: {
    presence: {
      allowEmpty: false,
      message: "^Email required",
    },
    email: {
      message: "^Email address must be valid",
    },
  },
  name: {
    presence: {
      allowEmpty: false,
      message: "^Name required",
    },
  },
  username: {
    presence: {
      allowEmpty: false,
      message: "^Username required"
    }
  },
  password: {
    presence: {
      allowEmpty: false,
      message: "^Password required",
    },
    length: {
      minimum: 8,
      message: "^Password must be at least 8 characters long",
    },
  },
  confirmPassword: {
    presence: {
      allowEmpty: false,
      message: "^Password confirmation required",
    },
    equality: {
      attribute: "password",
      message: "^Passwords are not equal",
    },
  },
};
