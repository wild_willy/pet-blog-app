import {validate} from "validate.js";
import {validationDictionary} from "./dictionary";

export interface Input<T> {
  type: string;
  value: T;
  optional?: boolean;
  errorLabel?: string;
}

// Validates an input
export function validateInput<T>(input: Input<T>) {
  const type = input.type;
  const value = input.value;
  const result = validate(
    {[type]: value},
    {[type]: validationDictionary[type]},
  );
  if (result) {
    return result[type][0];
  }
  return null;
}

// Determines the validation state of an input
export function getInputValidationState<T>(input: Input<T>, value: T) {
  return {
    ...input,
    value,
    errorLabel: input.optional
      ? null
      : validateInput({type: input.type, value}),
  };
}

// Validate multiple inputs at the same time iterates over the input array.
export function validateMultipleInputs(inputs: Array<Input<any>>) {
  const inputsValidation: {[index: string]: string} = {};
  inputs.forEach((input: Input<any>) => {
    inputsValidation[input.type] = input.value;
  });
  return validate(inputsValidation, validationDictionary);
}
