
import * as React from 'react'
import { Button, GestureResponderEvent, NativeSyntheticEvent, StyleSheet, Text, TextInput, TextInputChangeEventData, View } from 'react-native';
import {NavigationEvents} from 'react-navigation';
import { LoginState } from '../store/login/types';
import { Tokens } from '../api/Api';

// Login Props extends the System State Store
export interface LoginComponentProps extends LoginState {
  navigation: any;
  onLogin(loginEmail: string, loginPass: string): any;
  onSuccessfulLogin(tokens: Tokens, userId: number): any;
  onLogout(): void;
}

// Login component internal state
export interface LoginComponentState {
  loginEmail: string;
  loginPass: string;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5,
  },
  loginContainer: {
    justifyContent: "center",
  },
  loginInput: {
    height: 40,
    width: 90,
  },
  loginText: {
    fontSize: 15,
    textAlign: "center",
    margin: 10,
  },
});

export default class Login extends React.Component<LoginComponentProps, LoginComponentState> {
  constructor(props: LoginComponentProps){
    super(props);
    this.state = {
      loginEmail: "",
      loginPass: "",
    };
  }
  componentDidUpdate() {
    if(this.props.tokens){
      this.props.onSuccessfulLogin(this.props.tokens ?? {jwt: "", xsrf: ""}, this.props.profile?.authUserId ?? -1);
      this.handleFeed();
    }
  }

  handleInputEmail = (event: NativeSyntheticEvent<TextInputChangeEventData>) => {
    this.setState({
      ... this.state,
      loginEmail: event.nativeEvent.text,
    });
  }

  handleInputPass = (event: NativeSyntheticEvent<TextInputChangeEventData>) => {
    this.setState({
      ["loginPass"]: event.nativeEvent.text,
    });
  }

  handleSubmit = (event: GestureResponderEvent) => {
    event.preventDefault();
    if (this.state.loginEmail.trim() && this.state.loginPass.trim()) {
      this.props.onLogin(this.state.loginEmail, this.state.loginPass);
    }
  }
  handleReset = () => {
    this.setState({
      loginEmail: "",
      loginPass: "",
    });
  }

  handleCreateUser = () => {
    const {navigate} = this.props.navigation;
    navigate("CreateUserScreen");
  }

  handleFeed = () => {
    const {navigate} = this.props.navigation;
    navigate("Personal");
  }
  render() {
    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={payload => { this.handleReset(); this.props.onLogout();}}/>
        <Text style={styles.welcome}>
          Welcome to the Pet Blog App!
        </Text>
         <View style={styles.loginContainer}>
           <TextInput
             style={styles.loginInput}
             autoCapitalize={"none"}
             autoCorrect={false}
             onChange={this.handleInputEmail}
             value={this.state.loginEmail}
           />
            <Text style={styles.loginText}>
              Username
            </Text>
              <TextInput
                style={styles.loginInput}
                autoCapitalize={"none"}
                autoCorrect={false}
                secureTextEntry={true}
                onChange={this.handleInputPass}
                value={this.state.loginPass}
              />
            <Text style={styles.loginText}>
                Password
              </Text>
            <Button
              onPress={this.handleSubmit}
              title="Login"
            />
        </View>
        <Text style={styles.instructions}>
          If you are new here:
        </Text>
          <Button
            onPress={this.handleCreateUser}
            title="Sign Up"
          />
      </View>
    );
  }
}
