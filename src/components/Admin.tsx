import * as React from 'react';
import { View, FlatList } from 'react-native';
import { UserProfile, Tokens, AuthUser } from '../api/Api';
import User from './User';
import { NavigationStackProp } from 'react-navigation-stack';

// Admin Props
export interface AdminProps {
    users: UserProfile[] | undefined;
    navigation: NavigationStackProp;
    tokens: Tokens;
    getUsers(tokens: Tokens): void;
    deleteUserById(tokens: Tokens, userId: number): void;
    authUser: AuthUser | undefined;
}

// Admin State
export interface AdminState {

}

export default class Admin extends React.Component<AdminProps, AdminState> {
    handleFeed = () => {
        this.props.getUsers(this.props.tokens);
    }
    handleDeleteUser = (userId: number) => {
        this.props.deleteUserById(this.props.tokens, userId);
        this.handleFeed();
    }
    componentDidMount() {
        this.handleFeed();
    }
    render(){
        return (
            <View style={{flex: 1}}>
                    <FlatList
                        data={this.props.users ?? []}
                        keyExtractor = {item => item.profileUserId.toString()}
                        renderItem= {
                            ({item}) => 
                             (<View style={{flex: 1}}>
                                 <User 
                                    adminId={this.props?.authUser?.authUserId ?? 0}
                                    username={item.profileUsername}
                                    userId={item.profileUserId}
                                    delete={() => this.handleDeleteUser(item.profileUserId)}/>
                                 </View>)}/>
            </View>
        );
    }
}