import * as React from 'react';
import { View, Alert, ActivityIndicator } from 'react-native';
import MicropostComponent, { MicropostType } from './Micropost';
import { Tokens } from '../api/Api';
import { FlatList } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { SubsFeedState, SubsFeedStateTags } from '../store/feed/subscriptions/types';
import { FAB } from 'react-native-paper';


// Personal Feed Props
export interface SubsFeedCProps {
    navigation: any;
    subsFeed: SubsFeedState;
    userId: number;
    tokens: Tokens;
    getPosts(tokens: Tokens): void;
    unsubscribe(tokens: Tokens, subscribedTo: number): void;
}

// Personal Feed State
interface SubsFeedCState {

}

export default class SubsFeed extends React.Component<SubsFeedCProps, SubsFeedCState> {
    handleError = () => {
        if(this.props.subsFeed.error && this.props.subsFeed.tag === SubsFeedStateTags.FETCH_ERROR_SUBS){
            Alert.alert(this.props.subsFeed.error);
        }
    }
    componentDidMount(){
        console.log("mounting personal feed");
        this.handleFeed();
    }


    handleFeed = () => {
        this.props.getPosts(this.props.tokens);
    }

    handleUnsubscribe = (subscribedTo: number) => {
        if(this.props.userId !== subscribedTo){
            this.props.unsubscribe(this.props.tokens, subscribedTo);
            this.handleFeed();
        }
        
    } 

    render() {
        if (this.props.subsFeed.tag === SubsFeedStateTags.FETCHING_POSTS){
            return (
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size='large' color='#00000ff'/>
                </View>
            );
        } else {
            return (
                <View style={{flex: 1, paddingTop: 40}}>
                    <NavigationEvents onDidFocus={this.handleFeed}/>
                    <FlatList
                        data={this.props.subsFeed.posts ?? []}
                        keyExtractor = {item => item.id.toString()}
                        renderItem= {
                            ({item}) => 
                             (<View style={{flex: 1}}>
                                 <MicropostComponent 
                                    type={MicropostType.NonPersonal} 
                                    post={item}
                                    unsubscribe={() => this.handleUnsubscribe(item.author)}
                                 />
                                 </View>)}
                    />
                <FAB 
                        style={{position: 'absolute',
                                margin: 16,
                                left: 0,
                                bottom: 0}}
                        small
                        icon="cancel"
                        onPress={() => this.props.navigation.navigate('AuthStack')}/>
                </View>
            );
        }
    }
}
