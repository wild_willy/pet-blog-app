import * as React from "react";
import {
  Button,
  NativeSyntheticEvent,
  StyleSheet,
  Text,
  TextInput,
  TextInputChangeEventData,
  View,
  Alert,
} from "react-native";
import {Input, validateMultipleInputs} from "../validation/helpers";
import { UserState } from '../store/user/types';

interface Inputs {[index: string]: Input<any>; }

interface State {
  inputs: Inputs;
  valid: boolean;
  error: boolean;
}

interface Props extends UserState {
  onCreateUser(name: string, email: string, password: string, username: string): any;
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    backgroundColor: "#F5FCFF",
  },
  text: {
    fontSize: 20,
  },
  textInput: {
    height: 40,
  },
  error: {
    fontSize: 15,
    color: "red",
  },
});

export default class CreateUser extends React.Component<Props, State> {
  state: State = {
    inputs: {
      email: {
        type: "email",
        value: "",
      },
      name: {
        type: "name",
        value: "",
      },
      username: {
        type: "username",
        value: "",
      },
      password: {
        type: "password",
        value: "",
      },
      confirmPassword: {
        type: "confirmPassword",
        value: "",
      },
    },
    valid: false,
    error: false
  };

  componentDidUpdate() {
    if(this.props.userId){
      Alert.alert("User created successfully");
    }
    if(this.props.error && !this.state.error){
      Alert.alert(this.props.error);
      this.state.error = true;
    }
  }
  handleCreateUser = () => {
    this.validateForm();
    if (this.state.valid) {
      console.log("Sending Create User request");
      const {inputs} = this.state;
      this.props.onCreateUser(
        inputs.name.value.trim(),
        inputs.email.value.trim(),
        inputs.password.value.trim(),
        inputs.username.value.trim(),
      );
      this.state.error = false;
    }
  }
  handleInputChange = (id: string) => {
    const {inputs} = this.state;
    return (event: NativeSyntheticEvent<TextInputChangeEventData>) => {
      const value = event.nativeEvent.text;
      var input: Input<any> = inputs[id];
      input.value = value;
      input.errorLabel = undefined;
      this.setState({
        inputs: {
          ...inputs,
          id: input
        },
      });
      this.validateForm();
    };
  }

  renderError = (id: string) => {
    const {inputs} = this.state;
    if (inputs[id].errorLabel) {
      return <Text style={styles.error}>{inputs[id].errorLabel}</Text>;
    }
    return null;
  }

  validateForm = () => {
    console.log("Validating fields");
    const {inputs} = this.state;
    var validationOutcome: {[index: string]: any} = {};
    var inputA: Input<any>[] = [];
    let isValid: boolean = true;
    var keys: string[] = [];
    var updatedInputs: Inputs = inputs;
    for (const [key, input] of Object.entries(inputs)) {
      inputA.push(input);
      keys.push(key);
    }
    validationOutcome = validateMultipleInputs(inputA);
    if(!validationOutcome){
      this.setState({... this.state, valid: isValid});
    }else {
      console.log(validationOutcome);
      keys.map((key:string) => {
        updatedInputs[key].errorLabel = validationOutcome?.[key]?.[0];
      });
      console.log(updatedInputs);
      this.setState({inputs: updatedInputs, valid: !isValid});
    }
    
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Email</Text>
        <TextInput
          style={styles.textInput}
          placeholder="e.g. ex@example.com"
          autoCapitalize={"none"}
          autoCorrect={false}
          onChange={this.handleInputChange("email")}
        />
        {this.renderError("email")}
        <Text style={styles.text}>Name</Text>
        <TextInput
          style={styles.textInput}
          autoCapitalize={"none"}
          autoCorrect={false}
          placeholder="e.g. Jhon Travolta"
          onChange={this.handleInputChange("name")}
        />
        {this.renderError("name")}
        <Text style={styles.text}>Username</Text>
        <TextInput
          style={styles.textInput}
          autoCapitalize={"none"}
          autoCorrect={false}
          placeholder="e.g. carlson"
          onChange={this.handleInputChange("username")}
        />
        {this.renderError("username")}
        <Text style={styles.text}>Password</Text>
        <TextInput
          style={styles.textInput}
          autoCapitalize={"none"}
          autoCorrect={false}
          placeholder="Must be 8 characters long"
          secureTextEntry={true}
          onChange={this.handleInputChange("password")}
        />
        {this.renderError("password")}
        <Text style={styles.text}>Confirm password</Text>
        <TextInput
          style={styles.textInput}
          autoCapitalize={"none"}
          autoCorrect={false}
          placeholder="Same as above"
          secureTextEntry={true}
          onChange={this.handleInputChange("confirmPassword")}
        />
        {this.renderError("confirmPassword")}
        <Button title="Submit" onPress={this.handleCreateUser} />
      </View>
    );
  }
}
