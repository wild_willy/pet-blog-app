import * as React from 'react';
import {  TextInput, Button, View, NativeSyntheticEvent, TextInputChangeEventData, Picker } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { NewMicropost, Tokens, Micropost } from '../api/Api';


// Props for the Edit Post component
export interface EditPostProps {
    navigation: NavigationStackProp;
    updatePost(tokens: Tokens, postId: number, newPost: NewMicropost): void;
    tokens: Tokens;

}

// State for the Edit post component
export interface EditPostState {
    postInput: string;
    public: boolean;
    postId: number;
}

export default class EditPost extends React.Component<EditPostProps, EditPostState> {
    constructor(props: EditPostProps){
        super(props);
        this.state = {
            postInput: (this.props.navigation.getParam('post') as Micropost).post,
            public: (this.props.navigation.getParam('post') as Micropost).public,
            postId: (this.props.navigation.getParam('post') as Micropost).id 
        };
    }
    handleInput = (event: NativeSyntheticEvent<TextInputChangeEventData>) => {
        this.setState({
           ... this.state,
           postInput: event.nativeEvent.text,
    });}

    handleSubmit = () => {
        const time: string = new Date().toISOString();
        const post: NewMicropost = {
            newPost: this.state.postInput,
            created: time,
            public: this.state.public
        }
        this.props.updatePost(this.props.tokens, this.state.postId, post);
        this.props.navigation.goBack();
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <TextInput 
                    value={this.state.postInput}
                    onChange={this.handleInput}/>
                <Picker
                    selectedValue={this.state.public}
                    style={{height: 50, width: 100}}
                    onValueChange={(itemValue: boolean, ) =>
                         this.setState({...this.state, public: itemValue})}>
                    <Picker.Item label="Public" value={true} />
                    <Picker.Item label="Private" value={false} />
                </Picker>
                <Button
                    title="Submit"
                    onPress={this.handleSubmit}
                />
            </View>
        );
    }
}