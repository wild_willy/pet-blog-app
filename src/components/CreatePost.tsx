import * as React from 'react';
import {  TextInput, Button, View, NativeSyntheticEvent, TextInputChangeEventData, Picker } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { NewMicropost, Tokens } from '../api/Api';


// Props for the Create Post component
export interface CreatePostProps {
    navigation: NavigationStackProp;
    createPost(tokens: Tokens, newPost: NewMicropost): void;
    tokens: Tokens;
}

// State for the create post component
export interface CreatePostState {
    postInput: string;
    public: boolean;
}

export default class CreatePost extends React.Component<CreatePostProps, CreatePostState> {
    constructor(props: CreatePostProps){
        super(props);
        this.state = {
            postInput: "",
            public: true
        };
    }
    handleInput = (event: NativeSyntheticEvent<TextInputChangeEventData>) => {
        this.setState({
           ... this.state,
           postInput: event.nativeEvent.text,
    });}

    handleSubmit = () => {
        const time: string = new Date().toISOString();
        const post: NewMicropost = {
            newPost: this.state.postInput,
            created: time,
            public: this.state.public
        }
        this.props.createPost(this.props.tokens, post);
        this.props.navigation.goBack();
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <TextInput 
                    value={this.state.postInput}
                    onChange={this.handleInput}/>
                <Picker
                    selectedValue={this.state.public}
                    style={{height: 50, width: 100}}
                    onValueChange={(itemValue: boolean, ) =>
                         this.setState({...this.state, public: itemValue})}>
                    <Picker.Item label="Public" value={true} />
                    <Picker.Item label="Private" value={false} />
                </Picker>
                <Button
                    title="Submit"
                    onPress={this.handleSubmit}
                />
            </View>
        );
    }
}