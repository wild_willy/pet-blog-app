import * as React from 'react';
import { View, Alert, ActivityIndicator } from 'react-native';
import MicropostComponent, { MicropostType } from './Micropost';
import { Tokens } from '../api/Api';
import { FlatList } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation';
import { PublicFeedState, PublicFeedTags } from '../store/feed/public/types'
import { FAB } from 'react-native-paper';


// Public Feed Props
export interface PublicFeedCProps {
    navigation: any;
    publicFeed: PublicFeedState;
    userId: number;
    tokens: Tokens;
    getPosts(tokens: Tokens): void;
    subscribe(tokens: Tokens, subscribedTo: number): void;
}

// Public Feed State
interface PublicFeedCState {

}

export default class PublicFeed extends React.Component<PublicFeedCProps, PublicFeedCState> {
    handleError = () => {
        if(this.props.publicFeed.error && this.props.publicFeed.tag === PublicFeedTags.FETCH_ERROR){
            Alert.alert(this.props.publicFeed.error);
        }
    }
    componentDidMount(){
        console.log("mounting public feed");
        this.handleFeed();
    }


    handleFeed = () => {
        this.props.getPosts(this.props.tokens);
    }

    handleSubscribe = (subscribedTo: number) => {
        if(this.props.userId !== subscribedTo){
            this.props.subscribe(this.props.tokens, subscribedTo);
            this.handleFeed();
        }
        
    } 
    render() {
        if (this.props.publicFeed.tag === PublicFeedTags.FETCHING_POSTS){
            return (
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size='large' color='#00000ff'/>
                </View>
            );
        } else {
            return (
                <View style={{flex: 1, paddingTop: 40}}>
                    <NavigationEvents onDidFocus={this.handleFeed}/>
                    <FlatList
                        data={this.props.publicFeed.posts ?? []}
                        keyExtractor = {item => item.id.toString()}
                        renderItem= {
                            ({item}) => 
                             (<View style={{flex: 1}}>
                                 <MicropostComponent 
                                    type={MicropostType.NonPersonal} 
                                    post={item}
                                    unsubscribe={() => this.handleSubscribe(item.author)}
                                 />
                                 </View>)}
                    />
                    <FAB 
                        style={{position: 'absolute',
                                margin: 16,
                                left: 0,
                                bottom: 0}}
                        small
                        icon="cancel"
                        onPress={() => this.props.navigation.navigate('AuthStack')}/>
                </View>
            );
        }
    }
}
