import * as React from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

// User's Props
export interface UserProps {
    username: string;
    userId: number;
    delete(): void;
    adminId: number;
}

export interface UserState {
}

export default class User extends React.Component<UserProps, UserState> {
    render(){
        return (
           <View style={{flex: 1}}>
               <View style={{flex: 8}}>
                    <Text>{this.props.username}</Text>
               </View>
               <View style={{flex: 1, flexDirection: 'column'}}>
                    <View style={{flex: 1}}>
                        {this.props.userId == this.props.adminId ? 
                        (<Icon name="face"/>) : 
                        (<Icon name="lock" onPress={() => this.props.delete()}/>) } 
                    </View>
                    <View style={{flex: 1}}>

                    </View>
               </View>
           </View> 
        );
    }
}