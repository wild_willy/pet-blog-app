import * as React from 'react';
import { View, Alert, ActivityIndicator } from 'react-native';
import MicropostComponent, { MicropostType } from './Micropost';
import { PersonalFeedState, PersonalFeedStateTag } from '../store/feed/personal/types';
import { Tokens, Micropost } from '../api/Api';
import { FlatList } from 'react-native-gesture-handler';
import {FAB} from 'react-native-paper';
import { NavigationEvents } from 'react-navigation';


// Personal Feed Props
export interface PersonalFeedCProps {
    navigation: any;
    personalFeed: PersonalFeedState;
    userId: number;
    tokens: Tokens;
    getPosts(tokens: Tokens, userId: number): void; 
    delete(tokens: Tokens, postId: number): void;
    logout(): void;
    isAdmin: boolean;
}

// Personal Feed State
interface PersonalFeedCState {

}

export default class PersonalFeed extends React.Component<PersonalFeedCProps, PersonalFeedCState> {
    handleError = () => {
        if(this.props.personalFeed.error && this.props.personalFeed.tag === PersonalFeedStateTag.FETCH_ERROR){
            Alert.alert(this.props.personalFeed.error);
        }
    }
    componentDidMount(){
        console.log("mounting personal feed");
        this.handleFeed();
    }


    handleFeed = () => {
        this.props.getPosts(this.props.tokens, this.props.userId);
    }

    handleCreatePost = () =>  {
        this.props.navigation.navigate('CreatePost');
    }

    handleDeletePost = (postId: number) => {
        this.props.delete(this.props.tokens, postId);
        this.handleFeed();
    }

    handleEditPost = (post: Micropost) => {
        this.props.navigation.navigate('EditPost', {post});
    }
    render() {
        if (this.props.personalFeed.tag === PersonalFeedStateTag.FETCHING_POSTS){
            return (
                <View style={{flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator size='large' color='#00000ff'/>
                </View>
            );
        } else {
            return (
                <View style={{flex: 1, paddingTop: 40}}>
                    <NavigationEvents onDidFocus={this.handleFeed}/>
                    <FlatList
                        data={this.props.personalFeed.posts ?? []}
                        keyExtractor = {item => item.id.toString()}
                        renderItem= {
                            ({item}) => 
                             (<View style={{flex: 1}}>
                                 <MicropostComponent 
                                 type={MicropostType.Personal} 
                                 post={item}
                                 edit={() => this.handleEditPost(item)}
                                 delete={() => this.handleDeletePost(item.id)}/>
                                 </View>)}
                    />
                    <FAB 
                        style={{position: 'absolute',
                                margin: 16,
                                right: 0,
                                bottom: 0}}
                        small
                        icon="plus"
                        onPress={this.handleCreatePost}/>
                    <FAB 
                        style={{position: 'absolute',
                                margin: 16,
                                left: 0,
                                bottom: 0}}
                        small
                        icon="cancel"
                        onPress={() => this.props.navigation.navigate('AuthStack')}/>
                    {this.props.isAdmin ? 
                    (<FAB
                        style={{position: 'absolute',
                                margin: 16,
                                left: 90,
                                bottom: 0}}
                        small
                        icon="gavel"
                        onPress={() => this.props.navigation.navigate('AdminUsers')}
                    />) : (<View></View>)}
                        
                </View>
            );
        }
    }
}
