import * as React from 'react';
import { View, Text } from 'react-native';
import { Micropost } from '../api/Api';
import { Icon } from 'react-native-elements';


// Type of micropost rendering
export enum MicropostType {
    Personal,
    NonPersonal
}

// Props for rendering a micropost
export interface MicropostProps {
    type: MicropostType;
    post: Micropost;
    delete?(): void | undefined;
    edit?(): void | undefined;
    subscribe?(): void | undefined;
    unsubscribe?(): void | undefined; 
}

// State for rendering a micropost
export interface MicropostState {}

// Component for rendering a micropost depending of whether or not it 
// will render on the Personal feed or the Subs and Public feed. The rendering will depend 
// on the Micropost type.
export default class MicropostComponent extends React.Component<MicropostProps, MicropostState> {
    render() {
        switch(this.props.type){
            case MicropostType.Personal:
                return (
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 8, flexDirection: 'column'}}>
                            <View style={{flex: 5, flexDirection: 'row'}}>
                                <View style={{flex: 7}}>
                                    <Text style={{fontSize: 20}}>{this.props.post.post}</Text>
                                </View>
                                <View style={{flex:1, flexDirection: 'column'}}>
                                    <View style={{flex:1}}>
                                        {this.props.post.public ? (
                                            <Icon
                                                name='public'/>
                                        ) : (
                                            <Icon
                                                name='vpn-lock'/>
                                        )}
                                    </View>
                                    <View style={{flex:1}}>
                                    </View>
                                </View>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={{fontSize: 10}}>{new Date(this.props.post.posted).toDateString()}</Text>
                            </View>

                        </View>
                        <View style={{flex: 2, flexDirection: 'column'}}>
                            <View style={{flex: 1}}>
                                <Icon 
                                    name='create'
                                    color='red'
                                    size={15}
                                    onPress={() => {if (this.props.edit) {this.props.edit()}}}/>
                            </View>
                            <View style={{flex: 1}}>
                                <Icon
                                    name='delete'
                                    color='red'
                                    size={15}
                                    onPress={() => {if (this.props.delete) {this.props.delete()}}}/>
                            </View>
                        </View>
                    </View>
                );
            case MicropostType.NonPersonal:
                return (<View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 8, flexDirection: 'column'}}>
                            <View style={{flex: 5, flexDirection: 'row'}}>
                                <View style={{flex: 7}}>
                                    <Text style={{fontSize: 20}}>{this.props.post.post}</Text>
                                </View>
                                <View style={{flex:1, flexDirection: 'column'}}>
                                    <View style={{flex:1}}>
                                        {this.props.post.public ? (
                                            <Icon
                                                name='public'/>
                                        ) : (
                                            <Icon
                                                name='vpn-lock'/>
                                        )}
                                    </View>
                                    <View style={{flex:1}}>
                                        <Icon 
                                            name="notifications-off"
                                            color='red'
                                            onPress={
                                            () => {
                                                if (this.props.unsubscribe) 
                                                {this.props.unsubscribe()}}}/>
                                    </View>
                                </View>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={{fontSize: 10}}>{new Date(this.props.post.posted).toDateString()}</Text>
                            </View>

                        </View>
                    </View>);
            default:
                return (<View></View>);
        }
    }
}