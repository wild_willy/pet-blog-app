import { AxiosInstance, AxiosPromise } from 'axios';
import axios from 'axios';

// Tokens for the API
export interface Tokens {
    jwt: string;
    xsrf: string;
}

// The URL of the API
const apiUrl: string = "http://localhost:8080";

// Extract the tokens from the header string for set-cookie
// Can we do something less Hack?
export function extractTokens(setCookie: string): Tokens {
        const jwt: string = setCookie.split(';')?.[0]?.replace("JWT-Cookie=", "") ?? "jwt";
        const xsrf: string = setCookie.split(';')?.[3]?.split(',')?.[1]?.replace("XSRF-TOKEN=", "") ?? "xsrf";
        return {
            jwt,
            xsrf
        }
}

// Create a Axios instance from the tokens
export function apiInstance(tokens: Tokens | undefined): AxiosInstance {
    return axios.create({
      baseURL: apiUrl,
      timeout: 1000,
      headers: {
        "Authorization" : "Bearer " + tokens?.jwt, 
      },
      responseType: 'json'
  });
}

// -----------------------------------------------------------------
//                  API Objects JSONs
// -----------------------------------------------------------------

// A new User to the api
export interface NewUser {
    name: string;
    email: string;
    unhashedPass: string;
    username: string;
}

// A Login Form to the API
export interface LoginForm {
    loginEmail: string;
    loginPass: string;
}

// An Authenticated user of the API
export interface AuthUser {
  authName: string;
  authEmail: string;
  authUserId: number;
  authRole: string;
}

// A Subscription between two users for the API
export interface Subscription {
    subscriber: number;
    subscribedTo: number;
    createdAt: string;
}

// A new micropost
export interface NewMicropost {
    newPost: string;
    created: string;
    public: boolean;
}

// A micropost
export interface Micropost {
    id: number;
    post: string;
    posted: string;
    author: number;
    public: boolean;
}

// User's Profile
export interface UserProfile {
    profileUsername: string;
    profileUserId: number;

}

// -----------------------------------------------------------------
//                     API Calls
// -----------------------------------------------------------------

// Login Api Call
export function apiLogin(loginForm: LoginForm): AxiosPromise<AuthUser> {
    return axios.post(`${apiUrl}/users/login`, loginForm);
}

// Create User Api Call
export function apiCreateUser(newUser: NewUser): AxiosPromise<number> {
    return axios.post(`${apiUrl}/users/new`, newUser);
}

// GET Subscriptions API Call
export function apiGetSubs(tokens: Tokens | undefined): AxiosPromise<Subscription[]> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.get('/subscription/get');
}

// POST Subscribe API Call
export function apiSubscribe(tokens: Tokens | undefined, subscribeTo: number): AxiosPromise<number> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.post('/subscription/new/' + subscribeTo);
} 

// POST Unsubscribe API Call
export function apiUnsubscribe(tokens: Tokens | undefined, subscribedTo: number): AxiosPromise<number> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.post('/subscription/delete/' + subscribedTo);
}

// GET Public Microposts API Call
export function apiGetPublicPosts(tokens: Tokens | undefined): AxiosPromise<Micropost[]> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.get('/micropost/public');
}

// GET Microposts by author's id
export function apiGetPostsAuthorId(tokens: Tokens | undefined, authorId: number): AxiosPromise<Micropost[]>{
    const api: AxiosInstance = apiInstance(tokens);
    return api.get('/micropost/author/' + authorId);
}

// POST Create a new micropost
export function apiCreatePost(tokens: Tokens | undefined, newPost: NewMicropost): AxiosPromise<number> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.post('/micropost/new', newPost);
}

// POST Update existing micropost
export function apiUpdatePost(tokens: Tokens | undefined, postId: number, newPost: NewMicropost): AxiosPromise<number> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.post('/micropost/update/' + postId, newPost);
}

// POST Delete existing micropost
export function apiDeletePost(tokens: Tokens, postId: number): AxiosPromise<number> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.post('/micropost/delete/' + postId);
}

// GET Users only for Admin
export function apiUserProfiles(tokens: Tokens): AxiosPromise<UserProfile[]> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.get('/users/');
}

// POST Delete User by ID
export function apiDeleteUser(tokens: Tokens, userId: number): AxiosPromise<void> {
    const api: AxiosInstance = apiInstance(tokens);
    return api.post('/users/delete/' + userId);
}