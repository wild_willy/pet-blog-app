import { PublicFeedTags, PublicFeedState, PublicFeedActions, PublicActionTypes, PublicFetchErrorPayload, PublicFetchedPostsPayload, PublicFetchingPostsPayload } from './types'

// Initial State of the Public Feed Store
const initialState: PublicFeedState = {
    tag: PublicFeedTags.INITIAL_STATE,
}

export function publicFeedReducer(state=initialState, action: PublicFeedActions): PublicFeedState {
    switch(action.type) {
        case PublicActionTypes.FETCH_ERROR:
            const fetchErrorPayload: PublicFetchErrorPayload = action.payload as PublicFetchErrorPayload;
            return {
                ...state,
                tag: PublicFeedTags.FETCH_ERROR,
                error: fetchErrorPayload.error
            };
        case PublicActionTypes.FETCHED_POSTS:
            const fetchedPostsPayload: PublicFetchedPostsPayload = action.payload as PublicFetchedPostsPayload;
            return {
                ...state,
                tag: PublicFeedTags.FETCHED_POSTS,
                posts: fetchedPostsPayload.posts
            };
        case PublicActionTypes.FETCHING_POSTS:
            const fetchingPostsPayload: PublicFetchingPostsPayload = action.payload as PublicFetchingPostsPayload;
            return {
                ...state,
                tag: PublicFeedTags.FETCHING_POSTS,
                tokens: fetchingPostsPayload.tokens
            };
        default:
            return state;
    }
}