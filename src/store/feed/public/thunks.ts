import { Tokens, apiGetPublicPosts, apiSubscribe } from '../../../api/Api';
import { fetchingPosts, fetchedPosts, fetchError } from './actions';
import { PublicFeedState, PublicFeedActions } from './types';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

// Thunk for getting the public posts
export const publicPosts = (tokens: Tokens ): ThunkAction<Promise<void>,PublicFeedState, {}, PublicFeedActions> => {
    return async (dispatch: ThunkDispatch<PublicFeedState, {}, PublicFeedActions>) => {
        dispatch(fetchingPosts(tokens));
        apiGetPublicPosts(tokens).then(
            response => {
                dispatch(fetchedPosts(response.data));
            }
        ).catch(
            error => {
                dispatch(fetchError(error));
            }
        );
    }
}

// Thunk for subscribing
export const subscribe = (tokens: Tokens, subscribedTo: number): ThunkAction<Promise<void>, PublicFeedState, {}, PublicFeedActions> => {
    return async (dispatch: ThunkDispatch<PublicFeedState, {}, PublicFeedActions>) => {
        apiSubscribe(tokens, subscribedTo).then(
            // TODO: How To handle this
        ).catch(error => {
            console.log(error)
        });
    }
}