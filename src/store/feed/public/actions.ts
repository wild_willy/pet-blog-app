import { PublicFetchErrorAction, PublicActionTypes, PublicFetchedPostsAction, PublicFetchingPostsAction } from './types';
import { Micropost, Tokens } from '../../../api/Api';

// Action creator for the fetch error action
export const fetchError = (error: string): PublicFetchErrorAction => {
    return {
        type: PublicActionTypes.FETCH_ERROR,
        payload: {
            error
        }
    }
}

// Action creator for the fetched posts
export const fetchedPosts = (posts: Micropost[]): PublicFetchedPostsAction => {
    return {
        type: PublicActionTypes.FETCHED_POSTS,
        payload: {
            posts
        }
    }
}

// Action creator for the fetching posts
export const fetchingPosts = (tokens: Tokens): PublicFetchingPostsAction => {
    return {
        type: PublicActionTypes.FETCHING_POSTS,
        payload: {
            tokens
        }
    }
}