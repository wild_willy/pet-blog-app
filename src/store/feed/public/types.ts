import { Tokens, Micropost } from '../../../api/Api'
import {Action} from '../../types'

// Public Feed State
export interface PublicFeedState {
    tag: PublicFeedTags;
    error?: string;
    tokens?: Tokens;
    posts?: Micropost[];
}

// Public Feed State Tags
export enum PublicFeedTags {
    INITIAL_STATE,
    FETCH_ERROR,
    FETCHED_POSTS,
    FETCHING_POSTS
}

// Public Feed Action Types
export enum PublicActionTypes {
    FETCH_ERROR = '@@feed/public/FETCH_ERROR',
    FETCHING_POSTS = '@@feed/public/FETCHING_POSTS',
    FETCHED_POSTS = '@@feed/public/FETCHED_POSTS'
} 

// Generic public feed action
export type PublicFeedAction<P> = Action<PublicActionTypes, P>;

// Fetch Error Payload
export interface PublicFetchErrorPayload {
    error: string
}

// Public fetch error action
export type PublicFetchErrorAction = PublicFeedAction<PublicFetchErrorPayload>;

// Fetched Posts Payload
export interface PublicFetchedPostsPayload {
    posts: Micropost[]
}

// Public fetched posts action
export type PublicFetchedPostsAction = PublicFeedAction<PublicFetchedPostsPayload>;

// Fetching posts payload
export interface PublicFetchingPostsPayload {
    tokens: Tokens
}

// Public fetching posts action
export type PublicFetchingPostsAction = PublicFeedAction<PublicFetchingPostsPayload>;


// Public feed action
export type PublicFeedActions = 
    PublicFetchErrorAction
    | PublicFetchedPostsAction
    | PublicFetchingPostsAction