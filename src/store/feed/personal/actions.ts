import { FetchErrorAction, PersonalFeedActionTypes, FetchedPostsAction, FetchingPostsAction } from './types'
import { Micropost, Tokens } from "../../../api/Api";

// Action creator for Fetch Error
export const fetchError = (error: string): FetchErrorAction => {
    return {
        type: PersonalFeedActionTypes.FETCH_ERROR,
        payload: {
            error
        }
    }
}

// Action creator for Fetched Posts 
export const fetchedPosts = (posts: Micropost[]): FetchedPostsAction => {
    return {
        type: PersonalFeedActionTypes.FETCHED_POSTS,
        payload: {
            posts
        }
    }
}

// Action creator for Fetching Posts
export const fetchingPosts = (tokens: Tokens): FetchingPostsAction => {
    return {
        type: PersonalFeedActionTypes.FETCHING_POSTS,
        payload: {
            tokens
        }
    }
}