import { PersonalFeedState, PersonalFeedStateTag, PersonalFeedActions, PersonalFeedActionTypes, FetchErrorPayload, FetchedPostsPayload, FetchingPostsPayload } from './types'

// Initial State of the Personal Feed Store 
const initialState: PersonalFeedState = {
    tag: PersonalFeedStateTag.INITIAL_STATE
} 

// Reducer for the Personal Feed Store
export function personalFeedReducer(state = initialState, action: PersonalFeedActions): PersonalFeedState {
    switch(action.type) {
        case PersonalFeedActionTypes.FETCH_ERROR:
            const fetchErrorPayload: FetchErrorPayload = action.payload as FetchErrorPayload;
            return {... state, 
                error: fetchErrorPayload.error, 
                tag: PersonalFeedStateTag.FETCH_ERROR};
        case PersonalFeedActionTypes.FETCHED_POSTS:
            const fetchedPostsPayload: FetchedPostsPayload = action.payload as FetchedPostsPayload;
            return {... state, 
                posts: fetchedPostsPayload.posts, 
                tag: PersonalFeedStateTag.FETCHED_POSTS};
        case PersonalFeedActionTypes.FETCHING_POSTS:
            const fetchingPostsPayload: FetchingPostsPayload = action.payload as FetchingPostsPayload;
            return {... state, 
                tag: PersonalFeedStateTag.FETCHING_POSTS, 
                tokens: fetchingPostsPayload.tokens};
        default:
            return state;
    }
}