import { Tokens, Micropost } from '../../../api/Api'
import {Action} from '../../types'

// Personal Feed State 
export interface PersonalFeedState {
    tokens?: Tokens;
    error?: string;
    posts?: Micropost[];
    tag: PersonalFeedStateTag;
}

// Personal Feed State Tags
export enum PersonalFeedStateTag {
    INITIAL_STATE,
    FETCH_ERROR,    
    FETCHED_POSTS,
    FETCHING_POSTS
}

// Personal Feed Store action Types
export enum PersonalFeedActionTypes {
    FETCH_ERROR = '@@feed/personal/FETCH_ERROR',
    FETCHED_POSTS = '@@feed/personal/FETCHED_POSTS',
    FETCHING_POSTS = '@@feed/personal/FETCHING_POSTS'
}

// Personal Feed Store Action
export type PersonalFeedAction<P> = Action<PersonalFeedActionTypes, P>;

// Fetch error action payload
export interface FetchErrorPayload {
    error: string
}

// Fetch Error Action
export type FetchErrorAction = PersonalFeedAction<FetchErrorPayload>;

// Fetched posts action payload
export interface FetchedPostsPayload {
    posts: Micropost[]
}

// Fetched Posts action
export type FetchedPostsAction = PersonalFeedAction<FetchedPostsPayload>;

// Fetching posts action payload
export interface FetchingPostsPayload {
    tokens: Tokens
}

// Fetching posts action
export type FetchingPostsAction = PersonalFeedAction<FetchingPostsPayload>;

// Actions type
export type PersonalFeedActions =
    FetchErrorAction
    | FetchedPostsAction
    | FetchingPostsAction
