import { PersonalFeedState, PersonalFeedActions } from './types';
import { fetchingPosts, fetchedPosts, fetchError } from './actions';
import { Tokens, apiGetPostsAuthorId, NewMicropost, apiCreatePost, apiDeletePost, apiUpdatePost } from '../../../api/Api';
import { ThunkAction, ThunkDispatch } from 'redux-thunk'

// Thunk for getting the posts from user.
export const getPosts = (tokens: Tokens | undefined, userId: number): ThunkAction<Promise<void>, PersonalFeedState, {}, PersonalFeedActions> => {
    return async (dispatch: ThunkDispatch<PersonalFeedState, {}, PersonalFeedActions>) => {
        dispatch(fetchingPosts(tokens ?? {jwt: "", xsrf: ""}));
        apiGetPostsAuthorId(tokens, userId).then( response => {
            dispatch(fetchedPosts(response.data));
        }).catch(error => {
            dispatch(fetchError(error))
        });
    }
} 

// Thunk for posting
export const createPost = (tokens: Tokens | undefined, newPost: NewMicropost): ThunkAction<Promise<void>, PersonalFeedState, {}, PersonalFeedActions> => {
    return async (dispatch: ThunkDispatch<PersonalFeedState, {}, PersonalFeedActions>) => {
        apiCreatePost(tokens, newPost).then(
            // TODO What is the best way to handle this.
        ).catch(error => {
            console.log(error)
        }); //
    }
}

// Thunk for deleting a post
export const deletePost = (tokens: Tokens, postId: number): ThunkAction<Promise<void>, PersonalFeedState, {}, PersonalFeedActions> => {
    return async (dispatch: ThunkDispatch<PersonalFeedState, {}, PersonalFeedActions>) => {
        apiDeletePost(tokens, postId).then(
            // TODO: How To handle this
        ).catch(error => {
            console.log(error)
        });
    }
}

// Thunk for updating a post 
export const updatePost = (tokens: Tokens, postId: number, newPost: NewMicropost): ThunkAction<Promise<void>, PersonalFeedState, {}, PersonalFeedActions> => {
    return async (dispatch: ThunkDispatch<PersonalFeedState, {}, PersonalFeedActions>) => {
        apiUpdatePost(tokens, postId, newPost).then(
            // TODO: How To handle this
        ).catch(error => {
            console.log(error)
        });
    }
}