import { SubsFeedActions, SubsFeedStateTags, SubsFeedState, SubsFeedActionTypes, FetchErrorSubsPayload, FetchErrorPostsPayload, FetchingSubsPayload, FetchedSubsPayload, FetchedPostsPayload } from './types';
import { Micropost } from '../../../api/Api';

// Initial state of the Subs Store
const initialState: SubsFeedState = {
    tag: SubsFeedStateTags.INITIAL_STATE,
}

export function  subsFeedReducer(state=initialState, action: SubsFeedActions): SubsFeedState {
    switch(action.type) {
        case SubsFeedActionTypes.FETCH_ERROR_SUBS:
            const subsError: FetchErrorSubsPayload = action.payload as FetchErrorSubsPayload;
            return {
                ...state,
                tag: SubsFeedStateTags.FETCH_ERROR_SUBS,
                error: subsError.error
            }
        case SubsFeedActionTypes.FETCH_ERROR_POSTS:
            const postsError: FetchErrorPostsPayload = action.payload as FetchErrorPostsPayload;
            return {
                ...state,
                tag: SubsFeedStateTags.FETCH_ERROR_POSTS,
                error: postsError.error
            }
        case SubsFeedActionTypes.FETCHING_SUBS:
            const subsFetching: FetchingSubsPayload = action.payload as FetchingSubsPayload;
            return {
                ...state,
                tag: SubsFeedStateTags.FETCHING_SUBS,
                tokens: subsFetching.tokens
            }
        case SubsFeedActionTypes.FETCHING_POSTS:
            return {
                ...state,
                tag: SubsFeedStateTags.FETCHING_POSTS
            }
        case SubsFeedActionTypes.FETCHED_SUBS:
            const subsFetched: FetchedSubsPayload = action.payload as FetchedSubsPayload;
            return {
                ...state,
                tag: SubsFeedStateTags.FETCHED_SUBS,
                subs: subsFetched.subs
            }
        case SubsFeedActionTypes.FETCHED_POSTS:
            const postsFetched: FetchedPostsPayload = action.payload as FetchedPostsPayload;
            const oldPosts: Micropost[] = state.posts ?? [];
            const oldPostsIds: number[] = oldPosts.map(post => post.id);
            const newPosts: Micropost[] = postsFetched.posts.filter(post => !(oldPostsIds.includes(post.id)));
            return {
                ...state,
                tag: SubsFeedStateTags.FETCHED_POSTS,
                posts: oldPosts.concat(newPosts)
            }
        default:
            return state;
    }
}