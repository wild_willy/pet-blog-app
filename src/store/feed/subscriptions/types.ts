import {Tokens, Micropost, Subscription} from '../../../api/Api';
import { Action } from '../../types';

// Subscriptions Feed Store State
export interface SubsFeedState {
    tokens?: Tokens;
    posts?: Micropost[];
    subs?: Subscription[];
    error?: string;
    tag: SubsFeedStateTags
}

// State Tags
export enum SubsFeedStateTags {
    INITIAL_STATE,
    FETCH_ERROR_SUBS,
    FETCH_ERROR_POSTS,
    FETCHED_SUBS,
    FETCHED_POSTS,
    FETCHING_SUBS,
    FETCHING_POSTS
}

// Subs Feed Action Types
export enum SubsFeedActionTypes {
    FETCH_ERROR_SUBS = '@@feed/subs/FETCH_ERROR_SUBS',
    FETCH_ERROR_POSTS = '@@feed/subs/FETCH_ERROR_POSTS',
    FETCHED_SUBS = '@@feed/subs/FETCHED_SUBS',
    FETCHED_POSTS = '@@feed/subs/FETCHED_POSTS',
    FETCHING_SUBS = '@@feed/subs/FETCHING_SUBS',
    FETCHING_POSTS = '@@feed/subs/FETCHING_POSTS'
}

// Subs Feed Action Type Generic
export type SubsAction<P> = Action<SubsFeedActionTypes, P> 

// Subs Feed Fetch Error Subs Payload
export interface FetchErrorSubsPayload {
    error: string
}

// Subs Feed fetch error action
export type FetchErrorSubsAction = SubsAction< FetchErrorSubsPayload >;

// Posts Feed Fetch Error Subs Payload
export interface FetchErrorPostsPayload {
    error: string
}

// Posts Feed fetch error action
export type FetchErrorPostsAction = SubsAction< FetchErrorPostsPayload >;

// Subs fetching payload
export interface FetchingSubsPayload {
    tokens: Tokens
}

// Subs fetching action
export type FetchingSubsAction = SubsAction<FetchingSubsPayload>;

// Posts fetching payload
export interface FetchingPostsPayload {}

// Posts fetching action
export type FetchingPostsAction = SubsAction<FetchingPostsPayload>;

// Subs fetched Payload
export interface FetchedSubsPayload {
    subs: Subscription[]
}

// Subs fetched Action
export type FetchedSubsAction = SubsAction<FetchedSubsPayload>;

// Posts fetched Payload
export interface FetchedPostsPayload {
    posts: Micropost[]
}

// Subs fetched Action
export type FetchedPostsAction = SubsAction<FetchedPostsPayload>;

// Subs Feed Store Actions
export type SubsFeedActions = 
    FetchErrorSubsAction 
    | FetchErrorPostsAction 
    | FetchingPostsAction 
    | FetchingSubsAction 
    | FetchedSubsAction 
    | FetchedPostsAction