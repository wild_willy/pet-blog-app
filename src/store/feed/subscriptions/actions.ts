import { FetchErrorSubsAction, SubsFeedActionTypes, FetchErrorPostsAction, FetchingSubsAction, FetchingPostsAction, FetchedSubsAction, FetchedPostsAction } from './types';
import { Tokens, Subscription, Micropost } from '../../../api/Api';

// Action creator for fetch error subs
export const fetchErrorSubs = (error: string): FetchErrorSubsAction => {
    return {
        type: SubsFeedActionTypes.FETCH_ERROR_SUBS,
        payload: {
            error
        }
    }
}

// Action creator for fetch error posts
export const fetchErrorPosts = (error: string): FetchErrorPostsAction => {
    return {
        type: SubsFeedActionTypes.FETCH_ERROR_POSTS,
        payload: {
            error
        }
    }
}

// Action creator for fetching subs
export const fetchingSubs = (tokens: Tokens): FetchingSubsAction => {
    return {
        type: SubsFeedActionTypes.FETCHING_SUBS,
        payload: {
            tokens
        }
    }
}

// Action creator for fetching posts
export const fetchingPosts = (): FetchingPostsAction => {
    return {
        type: SubsFeedActionTypes.FETCHING_POSTS,
        payload: {}
    }
}

// Action creator for fetched subs 
export const fetchedSubs = (subs: Subscription[]): FetchedSubsAction => {
    return {
        type: SubsFeedActionTypes.FETCHED_SUBS,
        payload: {
            subs
        }
    }
}

// Action creator for fetched posts
export const fetchedPosts = (posts: Micropost[]): FetchedPostsAction => {
    return {
        type: SubsFeedActionTypes.FETCHED_POSTS,
        payload: {
            posts
        }
    }
}