import { Tokens, apiGetSubs, Subscription, apiGetPostsAuthorId, apiUnsubscribe } from '../../../api/Api';
import { SubsFeedActions, SubsFeedState } from './types';
import { fetchingSubs, fetchedPosts, fetchedSubs, fetchErrorSubs, fetchErrorPosts } from './actions';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

// Thunk for getting the posts from subscriptions
export const postsSubs = (tokens: Tokens): ThunkAction<Promise<void>, SubsFeedState, {}, SubsFeedActions> => {
    return async (dispatch: ThunkDispatch<SubsFeedState, {}, SubsFeedActions>) => {
        dispatch(fetchingSubs(tokens));
        apiGetSubs(tokens).then(
            response => {
                const subs: Subscription[] = response.data;
                dispatch(fetchedSubs(subs));
                subs.map((sub) => {
                    apiGetPostsAuthorId(tokens, sub.subscribedTo).then(resp => {
                        dispatch(fetchedPosts(resp.data));
                    }).catch( error => {
                        dispatch(fetchErrorPosts(error));
                    })
                })
            }
        ).catch(
            error => {
                dispatch(fetchErrorSubs(error));
            }
        );
    }
} 

// Thunk unsubscribing
export const unsubscribe = (tokens: Tokens, subscribedTo: number): ThunkAction<Promise<void>, SubsFeedState, {}, SubsFeedActions> => {
    return async (dispatch: ThunkDispatch<SubsFeedState, {}, SubsFeedActions>) => {
        apiUnsubscribe(tokens, subscribedTo).then(
            // TODO: How To handle this
        ).catch(error => {
            console.log(error)
        });
    }
}