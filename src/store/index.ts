import {userReducer} from './user/reducers';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import { loginReducer } from './login/reducer';
import { personalFeedReducer } from './feed/personal/reducer';
import { publicFeedReducer } from './feed/public/reducer';
import { subsFeedReducer } from './feed/subscriptions/reducer';
import { adminReducer } from './admin/reducer';

const rootReducer = combineReducers({
  login: loginReducer,
  user: userReducer,
  admin: adminReducer,
  personalFeed: personalFeedReducer,
  publicFeed: publicFeedReducer,
  subsFeed: subsFeedReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlers = [thunkMiddleware];
  const middleWareEnhancer = applyMiddleware(...middlers);
  const store = createStore(rootReducer, middleWareEnhancer);
  return store;
}
