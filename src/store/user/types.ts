// The shape of a new user.
export interface NewUser {
  name: string;
  email: string;
  unhashedPass: string;
  username: string;
}

// Shape of the user store
export interface UserState {
  newUser?: NewUser;
  userId?: number;
  error?: string;
}

// Available action names
export const CREATE_USER_REQUEST = "CREATE_USER_RESQUEST";
export const CREATE_USER_ERROR = "CREATE_USER_ERROR";
export const CREATE_USER_SUCCESSFUL = "CREATE_USER_SUCCESSFUL";

// Available action interfaces
export interface CreateUserSuccessful {
  type: typeof CREATE_USER_SUCCESSFUL;
  payload: {
    userId: number;
    user: NewUser;
  };
}

export interface CreateUserError {
  type: typeof CREATE_USER_ERROR;
  payload: {
    error: string;
  };
}

export interface CreateUserRequest {
  type: typeof CREATE_USER_REQUEST;
  payload: {
    user: NewUser;
  };
}

export type UserActionTypes = CreateUserSuccessful | CreateUserError | CreateUserRequest;
