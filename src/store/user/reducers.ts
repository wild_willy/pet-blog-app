import {
  CREATE_USER_ERROR,
  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESSFUL,
  UserActionTypes,
  UserState,
} from "./types";

// The initial state of the user store
const initialState = {};

export function userReducer(
  state = initialState,
  action: UserActionTypes,
): UserState {
  switch (action.type) {
    case CREATE_USER_SUCCESSFUL:
      return {
        newUser: action.payload.user,
        userId: action.payload.userId,
      };
    case CREATE_USER_ERROR:
      return {
        error: action.payload.error,
      };
    case CREATE_USER_REQUEST: {
      return {
        newUser: action.payload.user,
      };
    }
    default:
      return state;
  }
}
