import axios from "axios";
import {AnyAction} from "redux";
import {ThunkAction} from "redux-thunk";
import { createUserError, createUserRequest, createUserSuccessful } from "./actions";
import {NewUser} from "./types";

const apiURL = "http://localhost:8080/users";

// Create User axios
export const createUser = (
  newUser: NewUser,
): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: any) => {
    dispatch(createUserRequest(newUser));
    return axios
      .post(`${apiURL}/new`, newUser)
      .then(reponse => {
        dispatch(createUserSuccessful(reponse.data, newUser));
      })
      .catch(error => {
        // Better Error Handling see: https://gist.github.com/fgilio/230ccd514e9381fafa51608fcf137253
        // Or see https://www.npmjs.com/package/axios-error
        dispatch(createUserError(error.response.status));
      });
  };
};
