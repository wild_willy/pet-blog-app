import { CREATE_USER_ERROR, CREATE_USER_REQUEST, CREATE_USER_SUCCESSFUL, CreateUserError, CreateUserRequest, CreateUserSuccessful, NewUser } from './types';

// A user was created succesfully
export function createUserSuccessful(userId: number, user: NewUser): CreateUserSuccessful {
  return {
    type: CREATE_USER_SUCCESSFUL,
    payload: {
      userId,
      user,
    },
  };
}

// An error occurred on the user creation
export function createUserError(error: string): CreateUserError {
  return {
    type: CREATE_USER_ERROR,
    payload: {
      error,
    },
  };
}

// A user creation request have been send
export function createUserRequest(user: NewUser): CreateUserRequest {
  return {
    type: CREATE_USER_REQUEST,
    payload: {
      user,
    },
  };
}
