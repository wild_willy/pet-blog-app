import { LoginState, LoginStateTag, LoginActions, LoginActionTypes, LoginErrorPayload, LoginRequestPayload, LoggedPayload } from './types';
import {} from '../../api/Api';

// Initial State of the Login Store
const initialState: LoginState = {
    tag: LoginStateTag.INITIAL_STATE,
}

// Reducer for the Login Store
export function loginReducer(state=initialState, action: LoginActions): LoginState {
    switch(action.type){
        case LoginActionTypes.LOGIN_ERROR:
            const loginErrorPayload: LoginErrorPayload = action.payload as LoginErrorPayload;
            return {
                ...state,
                error: loginErrorPayload.error,
                tag: LoginStateTag.LOGIN_ERROR
            }
        case LoginActionTypes.LOGIN_REQUEST:
            const loginRequestPayload: LoginRequestPayload = action.payload as LoginRequestPayload;
            return {
                ...state,
                tag: LoginStateTag.LOGIN_REQUEST,
                loginForm: loginRequestPayload.loginForm
            }
        case LoginActionTypes.LOGGED:
            const loggedPayload: LoggedPayload = action.payload as LoggedPayload;
            return {
                ...state,
                tag: LoginStateTag.LOGGED,
                tokens: loggedPayload.tokens,
                profile: loggedPayload.profile
            }
        case LoginActionTypes.LOGOUT:
            return initialState;
        default:
            return state;
    }
}