import { Tokens, LoginForm, AuthUser } from '../../api/Api';
import { Action } from '../types';

// Login's Store State
export interface LoginState {
    tokens?: Tokens;
    tag: LoginStateTag;
    loginForm?: LoginForm;
    profile?: AuthUser;
    error?: string;
}

// Tag for the Login State
export enum LoginStateTag {
    INITIAL_STATE,
    LOGGED,
    LOGIN_ERROR,
    LOGIN_REQUEST,
}

// Login action types
export enum LoginActionTypes {
    LOGIN_REQUEST = '@@login/LOGIN_REQUEST',
    LOGIN_ERROR = '@@login/LOGIN_ERROR',
    LOGGED = '@@login/LOGGED',
    LOGOUT = '@@login/LOGOUT'
}

// Generic Login Store Action
export type LoginAction<P> = Action<LoginActionTypes, P>;

// Login Error Action Payload
export interface LoginErrorPayload {
    error: string;
}

// Login Error Action
export type LoginErrorAction = LoginAction<LoginErrorPayload>;

// Login Request Action Payload
export interface LoginRequestPayload {
    loginForm: LoginForm
}

// Login request action 
export type LoginRequestAction = LoginAction<LoginRequestPayload>;

// Logged Action Payload
export interface LoggedPayload {
    tokens: Tokens;
    profile: AuthUser;
}

// Logged Action
export type LoggedAction = LoginAction<LoggedPayload>;

// Logout Action Payload
export interface LogoutPayload {}

// Logout Action
export type LogoutAction = LoginAction<LogoutPayload>;

// Login Store Actions
export type LoginActions = 
    LoginErrorAction
    | LoginRequestAction
    | LoggedAction
    | LogoutAction