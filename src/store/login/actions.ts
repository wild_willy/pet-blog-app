import { LoginErrorAction, LoginActionTypes, LoginRequestAction, LoggedAction, LogoutAction } from './types';
import { LoginForm, Tokens, AuthUser } from '../../api/Api';

// Action Creator for Login Error
export const loginError = (error: string): LoginErrorAction => {
    return {
        type: LoginActionTypes.LOGIN_ERROR,
        payload: {
            error
        }
    }
}

// Action Creator for login request
export const loginRequest = (loginForm: LoginForm): LoginRequestAction => {
    return {
        type: LoginActionTypes.LOGIN_REQUEST,
        payload: {
            loginForm
        }
    }
}

// Action Creator for logged
export const logged = (tokens: Tokens, profile: AuthUser): LoggedAction => {
    return {
        type: LoginActionTypes.LOGGED,
        payload: {
            tokens,
            profile
        }
    }
}

// Action creator for Logout
export const logout = (): LogoutAction => {
    return {
        type: LoginActionTypes.LOGOUT,
        payload: {}
    }
}