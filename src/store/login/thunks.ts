import { apiLogin, LoginForm, extractTokens, Tokens } from '../../api/Api';
import { loginRequest, logged, loginError } from './actions';
import { LoginState, LoginActions } from './types';
import { ThunkDispatch, ThunkAction } from 'redux-thunk';

// Thunk for login
export const login = (loginForm: LoginForm): ThunkAction<Promise<void>, LoginState, {}, LoginActions> => {
    return async (dispatch: ThunkDispatch<LoginState, {}, LoginActions>) => {
        dispatch(loginRequest(loginForm));
        apiLogin(loginForm).then(response => {
            const tokens: Tokens = extractTokens(response.headers["set-cookie"][0]);
            console.log(response.data);
            dispatch(logged(tokens, response.data));
        }).catch(error => {
            dispatch(loginError(error));
        })
    }
}