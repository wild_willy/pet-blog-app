import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { AdminState, AdminActions } from './types'
import { Tokens, apiUserProfiles, apiDeleteUser } from '../../api/Api'
import { fetchingUsers, fetchedUsers, fetchError } from './actions'

// Thunk for getting the users
export const getUsers = (tokens: Tokens): ThunkAction<Promise<void>, AdminState, {}, AdminActions> => {
    return async (dispatch: ThunkDispatch<AdminState, {}, AdminActions>) => {
        dispatch(fetchingUsers(tokens));
        apiUserProfiles(tokens).then(response => {
            console.log(response.data)
            dispatch(fetchedUsers(response.data))
        }).catch(error => {
            dispatch(fetchError(error));
        })
    }
}

// Thunk for deleting a user by id
export const deleteUser = (tokens: Tokens, userId: number): ThunkAction<Promise<void>, AdminState, {}, AdminActions> => {
    return async (dispatch: ThunkDispatch<AdminState, {}, AdminActions>) => {
        apiDeleteUser(tokens, userId).then().catch(error => {
            console.log(error);
        })
    }
}