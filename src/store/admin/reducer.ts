import { AdminState, AdminStateTag, AdminActions, AdminActionTypes, FetchErrorPayload, FetchedUsersPayload, FetchingUsersPayload } from './types'

// Initial State of the Personal Feed Store 
const initialState: AdminState = {
    tag: AdminStateTag.INITIAL_STATE
} 

// Reducer for the Personal Feed Store
export function adminReducer(state = initialState, action: AdminActions): AdminState {
    switch(action.type) {
        case AdminActionTypes.FETCH_ERROR:
            const fetchErrorPayload: FetchErrorPayload = action.payload as FetchErrorPayload;
            return {... state, 
                error: fetchErrorPayload.error, 
                tag: AdminStateTag.FETCH_ERROR};
        case AdminActionTypes.FETCHED_USERS:
            const FetchedUsersPayload: FetchedUsersPayload = action.payload as FetchedUsersPayload;
            return {... state, 
                users: FetchedUsersPayload.users,
                tag: AdminStateTag.FETCHED_USERS};
        case AdminActionTypes.FETCHING_USERS:
            const FetchingUsersPayload: FetchingUsersPayload = action.payload as FetchingUsersPayload;
            return {... state, 
                tag: AdminStateTag.FETCHING_USERS, 
                tokens: FetchingUsersPayload.tokens};
        default:
            return state;
    }
}