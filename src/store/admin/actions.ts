import { FetchErrorAction, AdminActionTypes, FetchedUsersAction, FetchingUsersAction } from './types'
import { UserProfile, Tokens } from "../../api/Api";

// Action creator for Fetch Error
export const fetchError = (error: string): FetchErrorAction => {
    return {
        type: AdminActionTypes.FETCH_ERROR,
        payload: {
            error
        }
    }
}

// Action creator for Fetched Posts 
export const fetchedUsers = (users: UserProfile[]): FetchedUsersAction => {
    return {
        type: AdminActionTypes.FETCHED_USERS,
        payload: {
            users
        }
    }
}

// Action creator for Fetching Posts
export const fetchingUsers = (tokens: Tokens): FetchingUsersAction => {
    return {
        type: AdminActionTypes.FETCHING_USERS,
        payload: {
            tokens
        }
    }
}