import { Tokens, UserProfile } from '../../api/Api'
import {Action} from '../types'

// Admin State 
export interface AdminState {
    tokens?: Tokens;
    error?: string;
    users?: UserProfile[];
    tag: AdminStateTag;
}

// Admin State Tags
export enum AdminStateTag {
    INITIAL_STATE,
    FETCH_ERROR,    
    FETCHED_USERS,
    FETCHING_USERS
}

// Admin Action Types
export enum AdminActionTypes {
    FETCH_ERROR = '@@admin/FETCH_ERROR',
    FETCHED_USERS = '@@admin/FETCHED_USERS',
    FETCHING_USERS = '@@admin/FETCHING_USERS'
}

// Admin Store Action
export type AdminAction<P> = Action<AdminActionTypes, P>;

// Fetch error action payload
export interface FetchErrorPayload {
    error: string
}

// Fetch Error Action
export type FetchErrorAction = AdminAction<FetchErrorPayload>;

// Fetched posts action payload
export interface FetchedUsersPayload {
    users: UserProfile[]
}

// Fetched Posts action
export type FetchedUsersAction = AdminAction<FetchedUsersPayload>;

// Fetching posts action payload
export interface FetchingUsersPayload {
    tokens: Tokens
}

// Fetching posts action
export type FetchingUsersAction = AdminAction<FetchingUsersPayload>;

// Actions type
export type AdminActions =
    FetchErrorAction
    | FetchedUsersAction
    | FetchingUsersAction
