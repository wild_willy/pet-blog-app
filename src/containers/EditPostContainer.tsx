import EditPost from '../components/EditPost';
import {connect} from "react-redux"
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { NewMicropost, Tokens } from '../api/Api';
import { updatePost } from '../store/feed/personal/thunks';
import { PersonalFeedState } from '../store/feed/personal/types';

const mapStateToProps = (state: AppState) => {
    return {tokens: state.login.tokens ?? {jwt:"", xsrf:""}};
}

const mapDispatchToProps = (dispatch: ThunkDispatch<PersonalFeedState, {}, AnyAction>) => {
    return {
        updatePost: (tokens: Tokens, postId: number, newPost: NewMicropost) => {
            dispatch(updatePost(tokens, postId, newPost));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);