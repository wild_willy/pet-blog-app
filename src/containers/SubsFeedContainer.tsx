import {connect} from 'react-redux';
import {AppState} from './../store/index';
import SubsFeed from '../components/SubsFeed';
import { ThunkDispatch } from 'redux-thunk';
import { Tokens} from '../api/Api';
import { postsSubs, unsubscribe } from '../store/feed/subscriptions/thunks';
import { SubsFeedState, SubsFeedActions } from '../store/feed/subscriptions/types';

const mapStateToProps = (state: AppState) => {
    return {
        subsFeed: state.subsFeed,
        userId: state.login.profile?.authUserId ?? -1,
        tokens: state.login.tokens ?? {jwt:"", xsrf:""}
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<SubsFeedState, {}, SubsFeedActions>) => {
    return {
        getPosts(tokens: Tokens){
            dispatch(postsSubs(tokens));
        },
        unsubscribe(tokens: Tokens, subscribedTo: number){
            dispatch(unsubscribe(tokens, subscribedTo));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubsFeed)