import {connect} from "react-redux";
import Login from "../components/Login";
import {login} from '../store/login/thunks';
import { AppState } from '../store';
import { LoginForm, Tokens } from '../api/Api';
import { getPosts } from '../store/feed/personal/thunks';
import { logout } from '../store/login/actions';

const mapStateToProps = (state: AppState) => {
    return state.login;
} 
const mapDispatchToProps = (dispatch: any) => {
  return {
    onLogin: (loginEmail: string, loginPass: string) => {
      const loginForm: LoginForm = {
        loginEmail,
        loginPass
      }
      dispatch(login(loginForm));
    },
    onSuccessfulLogin: (tokens: Tokens, userId: number) => {
      dispatch(getPosts(tokens, userId));
    },
    onLogout: () => {
      dispatch(logout());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
