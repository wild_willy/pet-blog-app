import CreatePost from '../components/CreatePost';
import {connect} from "react-redux"
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { NewMicropost, Tokens } from '../api/Api';
import { createPost } from '../store/feed/personal/thunks';
import { PersonalFeedState } from '../store/feed/personal/types';

const mapStateToProps = (state: AppState) => {
    return {tokens: state.login.tokens ?? {jwt:"", xsrf:""}};
}

const mapDispatchToProps = (dispatch: ThunkDispatch<PersonalFeedState, {}, AnyAction>) => {
    return {
        createPost: (tokens: Tokens, newPost: NewMicropost) => {
            dispatch(createPost(tokens, newPost));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);