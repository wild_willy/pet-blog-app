import Admin from '../components/Admin';
import { getUsers, deleteUser } from '../store/admin/thunks';
import { AdminState, AdminActions } from '../store/admin/types';
import {AppState} from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { Tokens } from '../api/Api';


const mapStateToProps = (state: AppState) => {
    return {
        tokens: state.login.tokens ?? {jwt: "", xsrf: ""},
        users: state.admin.users,
        authUser: state.login.profile,
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<AdminState, {}, AdminActions>) => {
    return {
        getUsers(tokens: Tokens){
            dispatch(getUsers(tokens));
        },
        deleteUserById(tokens: Tokens, userId: number){
            dispatch(deleteUser(tokens, userId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin)