import {connect} from "react-redux";
import CreateUser from "../components/CreateUser";
import {createUser} from "../store/user/thunks";
import {NewUser} from "../store/user/types";
import { AppState } from '../store';

const mapStateToProps = (state: AppState) => {
  return state.user;
}
const mapDispatchToProps = (dispatch: any) => {
  return {
    onCreateUser: (name: string, email: string, unhashedPass: string, username: string)=> {
      const newUser: NewUser = {name, email, unhashedPass, username};
      dispatch(createUser(newUser));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateUser);
