import {connect} from 'react-redux';
import {AppState} from './../store/index';
import PersonalFeed, { } from '../components/PersonalFeed';
import { ThunkDispatch } from 'redux-thunk';
import { PersonalFeedState, PersonalFeedActions } from '../store/feed/personal/types';
import { Tokens} from '../api/Api';
import { getPosts, deletePost } from '../store/feed/personal/thunks';

const mapStateToProps = (state: AppState) => {
    return {
        personalFeed: state.personalFeed,
        userId: state.login.profile?.authUserId ?? -1,
        tokens: state.login.tokens ?? {jwt:"", xsrf:""},
        isAdmin: (state.login.profile?.authRole ?? "Normal") === "Admin"
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<PersonalFeedState, {}, PersonalFeedActions>) => {
    return {
        getPosts(tokens: Tokens, userId: number){
            dispatch(getPosts(tokens, userId));
        },
        delete(tokens: Tokens, postId: number){
            dispatch(deletePost(tokens, postId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PersonalFeed)