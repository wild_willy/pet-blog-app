import {connect} from 'react-redux';
import {AppState} from './../store/index';
import { ThunkDispatch } from 'redux-thunk';
import { Tokens} from '../api/Api';
import { publicPosts, subscribe } from '../store/feed/public/thunks';
import { PublicFeedState, PublicFeedActions } from '../store/feed/public/types';
import PublicFeed from '../components/PublicFeed';

const mapStateToProps = (state: AppState) => {
    return {
        publicFeed: state.publicFeed,
        userId: state.login.profile?.authUserId ?? -1,
        tokens: state.login.tokens ?? {jwt:"", xsrf:""}
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<PublicFeedState, {}, PublicFeedActions>) => {
    return {
        getPosts(tokens: Tokens){
            dispatch(publicPosts(tokens));
        },
        subscribe(tokens: Tokens, subscribedTo: number){
            dispatch(subscribe(tokens, subscribedTo));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicFeed)