import configureMockStore from "redux-mock-store";
import thunk, { ThunkMiddleware } from "redux-thunk";

export const mockStore = configureMockStore([thunk as ThunkMiddleware<any, any>]);
