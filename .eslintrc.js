module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "@react-native-community"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "project": "tsconfig.json",
        "sourceType": "module"
    },
    "plugins": [
        "@typescript-eslint",
        "@typescript-eslint/tslint"
    ],
    "root": true,
    "rules": {
        "comma-dangle": [
            1,
            "always-multiline"
        ],
        "no-cond-assign": "error",
        "no-console": "off",
        "no-const-assign": 2,
        "no-constant-condition": "off",
        "no-control-regex": 1,
        "no-debugger": "error",
        "no-dupe-class-members": 2,
        "no-dupe-keys": 2,
        "no-empty": "off",
        "no-ex-assign": 1,
        "no-extra-boolean-cast": 1,
        "no-extra-parens": 0,
        "no-extra-semi": 1,
        "no-func-assign": 1,
        "no-inner-declarations": 0,
        "no-invalid-regexp": 1,
        "no-negated-in-lhs": 1,
        "no-obj-calls": 1,
        "no-regex-spaces": 1,
        "no-reserved-keys": 0,
        "no-sparse-arrays": "error",
        "no-unreachable": 2,
        "use-isnan": "error",
        "valid-jsdoc": 0,
        "valid-typeof": "off",
        "block-scoped-var": 0,
        "complexity": "off",
        "consistent-return": 0,
        "curly": "error",
        "default-case": 0,
        "dot-notation": "error",
        "eqeqeq": [
            "error",
            "smart"
        ],
        "guard-for-in": "error",
        "no-alert": 1,
        "no-caller": "error",
        "no-div-regex": 1,
        "no-else-return": 0,
        "no-eq-null": 0,
        "no-eval": "error",
        "no-extend-native": 1,
        "no-extra-bind": "error",
        "no-fallthrough": "off",
        "no-floating-decimal": 1,
        "no-implied-eval": 1,
        "no-labels": 1,
        "no-iterator": 1,
        "no-lone-blocks": 1,
        "no-loop-func": 0,
        "no-multi-str": 0,
        "no-native-reassign": 0,
        "no-new": 1,
        "no-new-func": "error",
        "no-new-wrappers": "error",
        "no-octal": 1,
        "no-octal-escape": 1,
        "no-proto": 1,
        "no-redeclare": "error",
        "no-return-assign": 1,
        "no-script-url": 1,
        "no-self-compare": 1,
        "no-sequences": "error",
        "no-unused-expressions": [
            "error",
            {
                "allowShortCircuit": true
            }
        ],
        "no-void": 1,
        "no-warning-comments": 0,
        "no-with": 1,
        "radix": "error",
        "semi-spacing": 1,
        "vars-on-top": 0,
        "wrap-iife": 0,
        "yoda": 1,
        "no-catch-shadow": 1,
        "no-delete-var": 1,
        "no-label-var": 1,
        "no-shadow": [
            "error",
            {
                "hoist": "all"
            }
        ],
        "no-shadow-restricted-names": 1,
        "no-undef": 2,
        "no-undefined": 0,
        "no-undef-init": "error",
        "no-unused-vars": [
            1,
            {
                "vars": "all",
                "args": "none",
                "ignoreRestSiblings": true
            }
        ],
        "no-use-before-define": 0,
        "handle-callback-err": 1,
        "no-mixed-requires": 1,
        "no-new-require": 1,
        "no-path-concat": 1,
        "no-process-exit": 0,
        "no-restricted-modules": 1,
        "no-sync": 0,
        "eslint-comments/no-aggregating-enable": 1,
        "eslint-comments/no-unlimited-disable": 1,
        "eslint-comments/no-unused-disable": 1,
        "eslint-comments/no-unused-enable": 1,
        "prettier/prettier": 2,
        "key-spacing": 0,
        "keyword-spacing": 1,
        "jsx-quotes": [
            1,
            "prefer-double"
        ],
        "comma-spacing": 0,
        "no-multi-spaces": 0,
        "brace-style": 0,
        "camelcase": "off",
        "consistent-this": 1,
        "eol-last": "error",
        "func-names": 0,
        "func-style": 0,
        "new-cap": 0,
        "new-parens": "error",
        "no-nested-ternary": 0,
        "no-array-constructor": 1,
        "no-empty-character-class": 1,
        "no-lonely-if": 0,
        "no-new-object": 1,
        "no-spaced-func": 1,
        "no-ternary": 0,
        "no-trailing-spaces": [
            "error",
            {
                "ignoreComments": true
            }
        ],
        "no-underscore-dangle": "off",
        "no-mixed-spaces-and-tabs": 1,
        "quotes": [
            1,
            "single",
            "avoid-escape"
        ],
        "quote-props": [
            "error",
            "as-needed"
        ],
        "semi": 1,
        "sort-vars": 0,
        "space-in-brackets": 0,
        "space-in-parens": 0,
        "space-infix-ops": 1,
        "space-unary-ops": [
            1,
            {
                "words": true,
                "nonwords": false
            }
        ],
        "max-nested-callbacks": 0,
        "one-var": [
            "error",
            "never"
        ],
        "wrap-regex": 0,
        "max-depth": 0,
        "max-len": "off",
        "max-params": 0,
        "max-statements": 0,
        "no-bitwise": "error",
        "no-plusplus": 0,
        "react/display-name": 0,
        "react/jsx-boolean-value": 0,
        "react/jsx-no-comment-textnodes": 1,
        "react/jsx-no-duplicate-props": 2,
        "react/jsx-no-undef": 2,
        "react/jsx-sort-props": 0,
        "react/jsx-uses-react": 1,
        "react/jsx-uses-vars": 1,
        "react/no-did-mount-set-state": 1,
        "react/no-did-update-set-state": 1,
        "react/no-multi-comp": 0,
        "react/no-string-refs": 1,
        "react/no-unknown-property": 0,
        "react/prop-types": 0,
        "react/react-in-jsx-scope": 1,
        "react/self-closing-comp": 1,
        "react/wrap-multilines": 0,
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "error",
        "react-native/no-inline-styles": 1,
        "jest/no-disabled-tests": 1,
        "jest/no-focused-tests": 1,
        "jest/no-identical-title": 1,
        "jest/valid-expect": 1,
        "@typescript-eslint/adjacent-overload-signatures": "error",
        "@typescript-eslint/array-type": "error",
        "@typescript-eslint/ban-types": "off",
        "@typescript-eslint/class-name-casing": "error",
        "@typescript-eslint/consistent-type-assertions": "error",
        "@typescript-eslint/consistent-type-definitions": "error",
        "@typescript-eslint/explicit-member-accessibility": [
            "off",
            {
                "accessibility": "explicit"
            }
        ],
        "@typescript-eslint/indent": [
            "error",
            4,
            {
                "FunctionDeclaration": {
                    "parameters": "first"
                },
                "FunctionExpression": {
                    "parameters": "first"
                }
            }
        ],
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/member-delimiter-style": [
            "error",
            "error",
            {
                "multiline": {
                    "delimiter": "semi",
                    "requireLast": true
                },
                "singleline": {
                    "delimiter": "semi",
                    "requireLast": false
                }
            }
        ],
        "@typescript-eslint/member-ordering": "off",
        "@typescript-eslint/no-empty-function": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-misused-new": "error",
        "@typescript-eslint/no-namespace": "off",
        "@typescript-eslint/no-non-null-assertion": "error",
        "@typescript-eslint/no-parameter-properties": "off",
        "@typescript-eslint/no-require-imports": "off",
        "@typescript-eslint/no-this-alias": "error",
        "@typescript-eslint/no-use-before-declare": "off",
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/prefer-for-of": "error",
        "@typescript-eslint/prefer-function-type": "error",
        "@typescript-eslint/prefer-namespace-keyword": "error",
        "@typescript-eslint/promise-function-async": "off",
        "@typescript-eslint/quotes": [
            "error",
            "double",
            {
                "avoidEscape": true
            }
        ],
        "@typescript-eslint/semi": [
            "error",
            "always"
        ],
        "@typescript-eslint/space-within-parens": [
            "error",
            "always"
        ],
        "@typescript-eslint/triple-slash-reference": "error",
        "@typescript-eslint/type-annotation-spacing": "error",
        "@typescript-eslint/unified-signatures": "error",
        "arrow-body-style": "error",
        "arrow-parens": [
            "off",
            "as-needed"
        ],
        "capitalized-comments": "error",
        "class-methods-use-this": "off",
        "constructor-super": "error",
        "id-blacklist": "off",
        "id-match": "off",
        "import/no-default-export": "off",
        "import/no-extraneous-dependencies": "off",
        "import/no-internal-modules": "off",
        "import/order": "error",
        "linebreak-style": [
            "error",
            "unix"
        ],
        "max-classes-per-file": "off",
        "no-duplicate-case": "error",
        "no-duplicate-imports": "off",
        "no-invalid-this": "off",
        "no-irregular-whitespace": "error",
        "no-magic-numbers": "off",
        "no-multiple-empty-lines": [
            "error",
            {
                "max": 1
            }
        ],
        "no-return-await": "error",
        "no-template-curly-in-string": "error",
        "no-throw-literal": "error",
        "no-unsafe-finally": "error",
        "no-unused-labels": "error",
        "no-var": "error",
        "object-shorthand": "error",
        "padding-line-between-statements": [
            "off",
            "error",
            {
                "blankLine": "always",
                "prev": "*",
                "next": "return"
            }
        ],
        "prefer-arrow/prefer-arrow-functions": "error",
        "prefer-const": "error",
        "prefer-object-spread": "error",
        "prefer-template": "error",
        "space-before-function-paren": [
            "error",
            {
                "anonymous": "never",
                "asyncArrow": "always",
                "constructor": "never",
                "method": "never",
                "named": "never"
            }
        ],
        "spaced-comment": "error",
        "@typescript-eslint/tslint/config": [
            "error",
            {
                "rules": {
                    "import-spacing": true,
                    "jsdoc-format": true,
                    "jsx-alignment": true,
                    "jsx-curly-spacing": [
                        true,
                        "never"
                    ],
                    "jsx-equals-spacing": [
                        true,
                        "never"
                    ],
                    "jsx-key": true,
                    "jsx-no-bind": true,
                    "jsx-no-lambda": true,
                    "jsx-no-string-ref": true,
                    "no-duplicate-case": true,
                    "no-extra-boolean-cast": true,
                    "no-extra-semi": true,
                    "no-multi-spaces": true,
                    "no-reference-import": true,
                    "one-line": [
                        true,
                        "check-catch",
                        "check-else",
                        "check-finally",
                        "check-open-brace",
                        "check-whitespace"
                    ],
                    "prefer-conditional-expression": true,
                    "ter-indent": [
                        true,
                        2,
                        {
                            "SwitchCase": 1
                        }
                    ],
                    "trailing-comma": [
                        true,
                        {
                            "singleline": "never",
                            "multiline": "always"
                        }
                    ],
                    "typedef": [
                        true,
                        "property-declaration"
                    ],
                    "whitespace": [
                        true,
                        "check-branch",
                        "check-operator",
                        "check-typecast",
                        "check-separator",
                        "check-type",
                        "check-preblock"
                    ]
                }
            }
        ]
    },
    "settings": {
        "react": {
            "version": "detect"
        }
    }
};
